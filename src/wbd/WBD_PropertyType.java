package wbd;

/**
 * These are the properties that are to be loaded from properties.xml. They
 * provide custom labels and UI details for the WBD app. Allows it to be easy to
 * swap out UI text and icons without touching code.
 * @author Shawn Felix <shawnfelixcodes.wordpress.com>
 */
public enum WBD_PropertyType {
        // LOADED FROM properties.xml
        PROP_APP_TITLE,
        
        // APPLICATION ICONS
        NEW_COURSE_ICON,
        LOAD_COURSE_ICON,
        SAVE_COURSE_ICON,
        VIEW_SCHEDULE_ICON,
        EXPORT_PAGE_ICON,
        DELETE_ICON,
        EXIT_ICON,
        ADD_ICON,
        MINUS_ICON,
        MOVE_UP_ICON,
        MOVE_DOWN_ICON,
        EDIT_ICON,
        //BOTTOM OF THE SCREEN ICONS
        PLAYERS_SCREEN_ICON,
        FANTASY_TEAMS_ICON,
        DRAFT_SCREEN_ICON,
        FANTASY_STANDINGS_ICON,
        MLB_TEAMS_ICON,
        
        // APPLICATION TOOLTIPS FOR BUTTONS
        NEW_COURSE_TOOLTIP,
        LOAD_COURSE_TOOLTIP,
        SAVE_COURSE_TOOLTIP,
        VIEW_SCHEDULE_TOOLTIP,
        EXPORT_PAGE_TOOLTIP,
        DELETE_TOOLTIP,
        EXIT_TOOLTIP,
        ADD_ITEM_TOOLTIP,
        REMOVE_ITEM_TOOLTIP,
        ADD_LECTURE_TOOLTIP,
        REMOVE_LECTURE_TOOLTIP,
        MOVE_UP_LECTURE_TOOLTIP,
        MOVE_DOWN_LECTURE_TOOLTIP,
        ADD_HW_TOOLTIP,
        REMOVE_HW_TOOLTIP, 
        EDIT_ITEM_TOOLTIP,
        
        
        //BOTTOM OF SCREEN BUTTONS TOOLTIPS
        PLAYERS_SCREEN_TOOLTIP,
        FANTASY_TEAMS_TOOLTIP,
        DRAFT_SCREEN_TOOLTIP,
        FANTASY_STANDINGS_TOOLTIP,
        MLB_TEAMS_TOOLTIP,
        
        //AVAILABLE PLAYERS SCREEN
        ADD_PLAYER_TOOLTIP,
        REMOVE_PLAYER_TOOLTIP,
        SEARCH_LABEL,
        
        //fantasy Teams SCREEN
        DRAFT_NAME_LABEL,
        SELECT_FANTASY_TEAM_LABEL,
        
        // FOR COURSE EDIT WORKSPACE
        COURSE_HEADING_LABEL,
        COURSE_INFO_LABEL,
        COURSE_SUBJECT_LABEL,
        COURSE_NUMBER_LABEL,
        COURSE_SEMESTER_LABEL,
        COURSE_YEAR_LABEL,
        COURSE_TITLE_LABEL,
        INSTRUCTOR_NAME_LABEL,
        INSTRUCTOR_URL_LABEL,
        PAGES_SELECTION_HEADING_LABEL,
        SCHEDULE_ITEMS_HEADING_LABEL,
        LECTURES_HEADING_LABEL,
        HWS_HEADING_LABEL,
        //
        FANTASY_TEAMS_LABEL,
        AVAILABLE_PLAYERS_LABEL,
        FANTASY_STANDINGS_LABEL,
        DRAFT_SUMMARY_LABEL,
        MLB_TEAMS_LABEL,
        STARTING_LINEUP_LABEL,
        TAXI_SQUAD_LABEL,
        
        
                
        // FOR SCHEDULE EDITING
        SCHEDULE_HEADING_LABEL,
        DATE_BOUNDARIES_LABEL,
        STARTING_MONDAY_LABEL,
        ENDING_FRIDAY_LABEL,
        LECTURE_DAY_SELECT_LABEL,
        
        // ERROR DIALOG MESSAGES
        START_DATE_AFTER_END_DATE_ERROR_MESSAGE,
        START_DATE_NOT_A_MONDAY_ERROR_MESSAGE,
        END_DATE_NOT_A_FRIDAY_ERROR_MESSAGE,
        ILLEGAL_DATE_MESSAGE,
        
        // AND VERIFICATION MESSAGES
        NEW_COURSE_CREATED_MESSAGE,
        COURSE_LOADED_MESSAGE,
        COURSE_SAVED_MESSAGE,
        SITE_EXPORTED_MESSAGE,
        SAVE_UNSAVED_WORK_MESSAGE,
        REMOVE_ITEM_MESSAGE
}
