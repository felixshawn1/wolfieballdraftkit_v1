/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package wbd;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Locale;
import wbd.error.ErrorHandler;
import wbd.gui.WBD_GUI;
import wbd.data.*;
import wbd.data.DraftDataManager;
import static wbd.WBD_StartupConstants.*;
import properties_manager.PropertiesManager;
import xml_utilities.InvalidXMLFileFormatException;
import javafx.application.Application;
import static javafx.application.Application.launch;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.layout.StackPane;
import javafx.stage.Stage;
import wbd.file.JsonDraftFileManager;

/**
 *
 * @author Shawn Felix
 */
public class WolfieBallDraft extends Application {
    //THE FULL USER INTERFACE
    WBD_GUI gui;
    
    @Override
    public void start(Stage primaryStage){
        
        ErrorHandler eH = ErrorHandler.getErrorHandler();
        
        // LOAD APP SETTINGS INTO THE GUI AND START IT UP
        boolean success = loadProperties();
        
        //START IF LOADING PROPERTIES IS A SUCCESS
        if (success){
            try{
            
            JsonDraftFileManager jsonFileManager = new JsonDraftFileManager();
            
            
            ArrayList<Player> alist = new ArrayList();
            ObservableList<Player> list = FXCollections.observableList(alist);
                
            list = jsonFileManager.loadPlayers(JSON_FILE_PATH_PITCHERS, JSON_FILE_PATH_HITTERS);
                
            /*gui.getDataManager().getDraft()
                        .setPlayersList(list);*/
            
//@todo exporting
            
            //Start up the gui
            gui = new WBD_GUI(primaryStage);
            
            
            gui.setDraftFileManager(jsonFileManager);
            //exporter goes here
            
            //INITIALIZE THE DATA MANAGER FOR THE GUI
            DraftDataManager dataManager = new DraftDataManager(gui);
            
            gui.setDataManager(dataManager);
            
            gui.initGUI("Wolfieball Draft Kit", list);
            
            }catch(IOException ioe){
                eH = ErrorHandler.getErrorHandler();
                eH.handlePropertiesFileError();
            }
            
        }
    }

    public boolean loadProperties() {
        try {
            // LOAD THE SETTINGS FOR STARTING THE APP
            PropertiesManager props = PropertiesManager.getPropertiesManager();
            props.addProperty(PropertiesManager.DATA_PATH_PROPERTY, PATH_DATA);
            props.loadProperties(PROPERTIES_FILE_NAME, PROPERTIES_SCHEMA_FILE_NAME);
            return true;
       } catch (InvalidXMLFileFormatException ixmlffe) {
            // SOMETHING WENT WRONG INITIALIZING THE XML FILE
            ErrorHandler eH = ErrorHandler.getErrorHandler();
            eH.handlePropertiesFileError();
            return false;
        }        
    }
    
    /**
     * @param args the command line arguments
     */
    public static void main(String[] args){
         Locale.setDefault(Locale.US);
         launch(args);
     }
    
}
