package wbd.data;

import java.util.ArrayList;
import java.util.Arrays;
import wbd.data.Player.*;

import javafx.beans.property.ObjectProperty;
import javafx.beans.property.SimpleObjectProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;

/**
 *
 * @author Shawn Felix <shawnfelixcodes.wordpress.com>
 */
public class Hitter extends Player{
    
     StringProperty atBat;       //ab
     StringProperty runs;        //r
     StringProperty hits;        //h
     StringProperty homeRuns;    //hr
     StringProperty rbis;        //rbis
     StringProperty stolenBases; //sb
     
     
     StringProperty battingAvg; // runs/at bats
    
    
    public Hitter(){
        atBat = new SimpleStringProperty();
        runs = new SimpleStringProperty();
        hits = new SimpleStringProperty();
        homeRuns = new SimpleStringProperty();
        rbis = new SimpleStringProperty();
        stolenBases = new SimpleStringProperty();

        battingAvg = new SimpleStringProperty();
    }
    
    
    public void updateHitterValues(){
        if(Double.valueOf(hits.get())!=0){
            setBattingAvg(String.valueOf(Double.valueOf(hits.get()) / Double.valueOf(atBat.get())).format("%.2f", Double.valueOf(hits.get()) / Double.valueOf(atBat.get())));
            setBawhip(String.valueOf(Double.valueOf(hits.get()) / Double.valueOf(atBat.get())).format("%.2f",Double.valueOf(hits.get()) / Double.valueOf(atBat.get())));
        }
        else{
            setBattingAvg("0");
            setBawhip("0");
        }
        
        
        positionsArray = new ArrayList<String>(Arrays.asList(positions.get().split("_")));

    }
    
    /**
     * @return the atBat
     */
    public StringProperty getAtBatProperty() {
        return atBat;
    }
    public String getAtBat(){
        return atBat.get();
    }

    /**
     * @param atBat the atBat to set
     */
    public void setAtBat(String atBat) {
        this.atBat.set(atBat);
    }

    /**
     * @return the runs
     */
    public StringProperty getRunsProperty() {
        return runs;
    }
    
    public String getRuns(){
        return runs.get();
    }

    /**
     * @param runs the runs to set
     */
    public void setRuns(String runs) {
        this.runs.set(runs);
    }

    /**
     * @return the hits
     */
    public StringProperty getHitsProperty() {
        return hits;
    }

    public String getHits(){
        return hits.get();
    }
    /**
     * @param hits the hits to set
     */
    public void setHits(String hits) {
        this.hits.set(hits);
    }

    /**
     * @return the homeRuns
     */
    public StringProperty getHomeRunsProperty() {
        return homeRuns;
    }

    public String getHomeRuns(){
        return homeRuns.get();
    }
    /**
     * @param homeRuns the homeRuns to set
     */
    public void setHomeRuns(String homeRuns) {
        this.homeRuns.set(homeRuns);
    }

    /**
     * @return the rbis
     */
    public StringProperty getRbisProperty() {
        return rbis;
    }
    
    public String getRbis(){
        return rbis.get();
    }

    /**
     * @param rbis the rbis to set
     */
    public void setRbis(String rbis) {
        this.rbis.set(rbis);
    }

    /**
     * @return the stolenBases
     */
    public StringProperty getStolenBasesProperty() {
        return stolenBases;
    }
    
    public String getStolenBases(){
        return stolenBases.get();
    }

    /**
     * @param stolenBases the stolenBases to set
     */
    public void setStolenBases(String stolenBases) {
        this.stolenBases.set(stolenBases);
    }
    
    /**
     * @return the battingAvg
     */
    public StringProperty getBattingAvgProperty() {
        return battingAvg;
    }
    
    public String getBattingAvg(){
        return battingAvg.get();
    }

    /**
     * @param battingAvg the stolenBases to set
     */
    public void setBattingAvg(String battingAvg) {
        this.battingAvg.set(battingAvg);
    }

    

}
