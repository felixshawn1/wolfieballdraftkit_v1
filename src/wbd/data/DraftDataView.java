package wbd.data;

/**
 * This type represents an abstraction of what our data manager
 * thinks of in regards to our GUI. The point is that we can
 * easily decouple these two by using such a narrow interface.
 * 
 * @author Shawn Felix <shawnfelixcodes.wordpress.com>
 */
public interface DraftDataView {
    public void reloadDraft(Draft draftToReload);
}
