package wbd.data;

import java.util.ArrayList;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import wbd.file.DraftFileManager;

/**
 *
 * @author Shawn Felix <shawnfelixcodes.wordpress.com>
 */
public class DraftDataManager {
    //THIS IS THE DRAFT BEING EDITED
    Draft draft;
    
    
    //THIS IS THE UI WHICH MUST BE UPDATED
    // WHENEVER OUR MODEL'S DATA CHANGES
    DraftDataView view;
    
    //LOADS THINGS FOR OUR DRAFT
    DraftFileManager fileManager;
    
    ObservableList<Player> playerObList;
    ObservableList<Hitter> hitterObList;
    ObservableList<Pitcher> pitcherObList;
    
    
    public DraftDataManager(DraftDataView initView){
        view = initView;
        draft = new Draft();
        
    }
    
    
    
    /**
     * Accessor method for getting the Draft that this class manages
     * @return draft
     */
    public Draft getDraft(){
        return draft;
    }
    
    /** Accessor method for getting the file manager, which knows how
     * to read and write course data from/to files.
     * @return fileManager
     */
    public DraftFileManager getFileManager(){
        return fileManager;
    }
    
    /**
     * Resets the draft to its default initialized settings, triggering
     * the UI to reflect these changes.
     */
    public void reset(){
        view.reloadDraft(draft);
    }
}
