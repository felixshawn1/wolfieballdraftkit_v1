package wbd.data;

import java.util.ArrayList;
import java.util.Arrays;
import javafx.beans.property.ObjectProperty;
import javafx.beans.property.SimpleObjectProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;

/**
 *
 * @author Shawn Felix <shawnfelixcodes.wordpress.com>
 */
public class Pitcher extends Player{
    
    private StringProperty inningsPitched;//ip
    private StringProperty earnedRuns;    //er
    private StringProperty wins;          //w
    private StringProperty saves;         //sv
    private StringProperty hits;         //h
    private StringProperty baseOnBalls;   //bb (walks)
    private StringProperty strikeouts;    //k
    private StringProperty earnedRunsAverage; //era 9*(earnedRuns / inningsPitched)
    private StringProperty whip;            //whip
    
    public Pitcher(){
        inningsPitched = new SimpleStringProperty();
        earnedRuns = new SimpleStringProperty();
        wins = new SimpleStringProperty();
        saves = new SimpleStringProperty();
        hits = new SimpleStringProperty();
        baseOnBalls = new SimpleStringProperty();
        strikeouts = new SimpleStringProperty();
        earnedRunsAverage = new SimpleStringProperty();
        whip = new SimpleStringProperty();
        
    }
    
    //updates the era and whip
    //THIS SHOULD BE CALLED EVERY TIME A CHANGE IS MADE TO A PLAYER
    public void updatePitcherValues(){
        double sberaa = 9 * (Double.valueOf(earnedRuns.get()) / Double.valueOf(inningsPitched.get()));
        double bawhipp = (Double.valueOf(baseOnBalls.get())+ Double.valueOf(hits.get()))/Double.valueOf(inningsPitched.get());
        //sets the earned runs average
        setEarnedRunsAverage(String.valueOf(9 * (Double.valueOf(earnedRuns.get()) / Double.valueOf(inningsPitched.get()))));
        //sets the whip
        setWhip(String.valueOf((Double.valueOf(baseOnBalls.get())+ Double.valueOf(hits.get()))/Double.valueOf(inningsPitched.get())));
    
        if(!earnedRuns.get().equals("0"))
            setSbera(String.valueOf(9 * (Double.valueOf(earnedRuns.get()) / Double.valueOf(inningsPitched.get()))).format("%.2f", Double.valueOf(sberaa)));
        else
            setSbera("0");
        
        if(!(Double.valueOf(baseOnBalls.get())+ Double.valueOf(hits.get()) == 0))
            setBawhip(String.valueOf((Double.valueOf(baseOnBalls.get())+ Double.valueOf(hits.get()))/Double.valueOf(inningsPitched.get())).format("%.2f", Double.valueOf(bawhipp)));
        else
            setBawhip("0");
        
        positionsArray = new ArrayList<String>(Arrays.asList(positions.get().split("_")));
        
    }
    

    /**
     * @return the inningsPitched
     */
    public String getInningsPitched() {
        return inningsPitched.get();
    }
    public StringProperty getInningsPitchedProperty(){
        return inningsPitched;
    }
    /**
     * @param inningsPitched the inningsPitched to set
     */
    public void setInningsPitched(String inningsPitched) {
        this.inningsPitched.set(inningsPitched);
    }
    

    /**
     * @return the earnedRuns
     */
    public String getEarnedRuns() {
        return earnedRuns.get();
    }
    public StringProperty getEarnedRunsProperty(){
        return earnedRuns;
    }

    /**
     * @param earnedRuns the earnedRuns to set
     */
    public void setEarnedRuns(String earnedRuns) {
        this.earnedRuns.set(earnedRuns);
    }

    /**
     * @return the wins
     */
    public String getWins() {
        return wins.get();
    }
    public StringProperty getWinsProperty(){
        return wins;
    }
    /**
     * @param wins the wins to set
     */
    public void setWins(String wins) {
        this.wins.set(wins);
    }

    /**
     * @return the saves
     */
    public String getSaves() {
        return saves.get();
    }
    public StringProperty getSavesProperty(){
        return saves;
    }
    /**
     * @param saves the saves to set
     */
    public void setSaves(String saves) {
        this.saves.set(saves);
    }

    /**
     * @return the hits
     */
    public String getHits() {
        return hits.get();
    }
    public StringProperty getHitsProperty(){
        return hits;
    }
    /**
     * @param hits the hits to set
     */
    public void setHits(String hits) {
        this.hits.set(hits);
    }

    /**
     * @return the baseOnBalls
     */
    public String getBaseOnBalls() {
        return baseOnBalls.get();
    }
    public StringProperty getBaseOnBallsProperty(){
        return baseOnBalls;
    }

    /**
     * @param baseOnBalls the baseOnBalls to set
     */
    public void setBaseOnBalls(String baseOnBalls) {
        this.baseOnBalls.set(baseOnBalls);
    }

    /**
     * @return the strikeouts
     */
    public String getStrikeouts() {
        return strikeouts.get();
    }
    
    public StringProperty getStrikeoutsProperty(){
        return strikeouts;
    }
    /**
     * @param strikeouts the strikeouts to set
     */
    public void setStrikeouts(String strikeouts) {
        this.strikeouts.set(strikeouts);
    }

    /**
     * @return the earnedRunsAverage
     */
    public String getEarnedRunsAverage() {
        return earnedRunsAverage.get();
    }
    public StringProperty getEarnedRunsAverageProperty(){
        return earnedRunsAverage;
    }

    /**
     * @param earnedRunsAverage the earnedRunsAverage to set
     */
    public void setEarnedRunsAverage(String earnedRunsAverage) {
        this.earnedRunsAverage.set(earnedRunsAverage);
    }
    
    
    /**
     * @return the whip
     */
    public String getWhip() {
        return whip.get();
    }
    public StringProperty getWhipProperty(){
        return whip;
    }

    /**
     * @param whip the earnedRunsAverage to set
     */
    public void setWhip(String whip) {
        this.whip.set(whip);
    }
       
    
}
