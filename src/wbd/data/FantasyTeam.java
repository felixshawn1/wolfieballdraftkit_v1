package wbd.data;

import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.scene.control.ComboBox;

/**
 *
 * @author Shawn Felix <shawnfelixcodes.wordpress.com>
 */
public class FantasyTeam {

    StringProperty teamName;
    StringProperty ownerName;

    StringProperty playersNeeded;
    StringProperty moneyLeft;
    StringProperty moneyPP;
    StringProperty runs;
    StringProperty homeRuns;
    StringProperty rbis;
    StringProperty stolenBases;
    StringProperty battingAverage;
    StringProperty wins;
    StringProperty saves;
    StringProperty strikeouts;
    StringProperty era;
    StringProperty whip;
    StringProperty totalPoints;
    ObservableList<String> positionsTaken;

    ObservableList<Player> players;
    ObservableList<Player> taxiPlayers;
    

    public FantasyTeam(String teamName, String ownerName) {
        this.teamName = new SimpleStringProperty();
        this.ownerName = new SimpleStringProperty();
        this.teamName.set(teamName);
        this.ownerName.set(ownerName);

        playersNeeded = new SimpleStringProperty();
        moneyLeft = new SimpleStringProperty();
        moneyPP = new SimpleStringProperty();
        runs = new SimpleStringProperty();
        homeRuns = new SimpleStringProperty();
        rbis = new SimpleStringProperty();
        stolenBases = new SimpleStringProperty();
        battingAverage = new SimpleStringProperty();
        wins = new SimpleStringProperty();
        saves = new SimpleStringProperty();
        strikeouts = new SimpleStringProperty();
        era = new SimpleStringProperty();
        whip = new SimpleStringProperty();
        totalPoints = new SimpleStringProperty();

        positionsTaken = FXCollections.observableArrayList();
        moneyLeft.set("260");
        playersNeeded.set("23");
        players = FXCollections.observableArrayList();
        taxiPlayers = FXCollections.observableArrayList();
    }

    public ObservableList<Player> getPlayersList() {
        return players;
    }

    public boolean isValidChoice(String pos){
        if(pos.contains("1B") && !positionsTaken.contains("1B")){
            return true;
        }
        else if(pos.contains("2B") && !positionsTaken.contains("2B")){
            return true;
        }
        else if(pos.contains("3B") && !positionsTaken.contains("3B")){
            return true;
        }
        else if(pos.contains("CI") && !positionsTaken.contains("CI")){
            return true;
        }
        else if(pos.contains("SS") && !positionsTaken.contains("SS")){
            return true;
        }
        else if(pos.contains("MI") && !positionsTaken.contains("MI")){
            return true;
        }
        else if(pos.contains("U") && !positionsTaken.contains("U")){
            return true;
        }
        else if(pos.contains("OF")){
            //allow up to 5 OF
            if(numberOfPlayers("OF")<5){
                return true;
            }
            else
                return false;
        }
        
        else if(pos.equals("C")){
            //allow up to two catchers
            if(numberOfPlayers("C")<2){
                return true;
            }
            else
                return false;
        }
        else if(pos.contains("P")){
            //allow up to 9 pitchers
            if(numberOfPlayers("P")<9){
                return true;
            }
            else
                return false;
        }
        else
            return false;
        
    }
    
    public boolean checkForValidPlayerChoice(String pos){
        
        if(pos.contains("1B") && !positionsTaken.contains("1B")){
            positionsTaken.add("1B");
            return true;
        }
        else if(pos.contains("2B") && !positionsTaken.contains("2B")){
            positionsTaken.add("2B");
            return true;
        }
        else if(pos.contains("3B") && !positionsTaken.contains("3B")){
            positionsTaken.add("3B");
            return true;
        }
        else if(pos.contains("CI") && !positionsTaken.contains("CI")){
            positionsTaken.add("CI");
            return true;
        }
        else if(pos.contains("SS") && !positionsTaken.contains("SS")){
            positionsTaken.add("SS");
            return true;
        }
        else if(pos.contains("MI") && !positionsTaken.contains("MI")){
            positionsTaken.add("MI");
            return true;
        }
        else if(pos.contains("U") && !positionsTaken.contains("U")){
            positionsTaken.add("U");
            return true;
        }
        else if(pos.contains("OF")){
            //allow up to 5 OF
            if(numberOfPlayers("OF")<5){
                positionsTaken.add("OF");
                return true;
            }
            else
                return false;
        }
        
        else if(pos.equals("C")){
            //allow up to two catchers
            if(numberOfPlayers("C")<2){
                positionsTaken.add("C");
                return true;
            }
            else
                return false;
        }
        else if(pos.contains("P")){
            //allow up to 9 pitchers
            if(numberOfPlayers("P")<9){
                positionsTaken.add("P");
                return true;
            }
            else
                return false;
        }
        else
            return false;
        
    }
    private int numberOfPlayers(String pos){
        int e = 0;
        for(int i = 0; i<positionsTaken.size(); i++){
            if(positionsTaken.get(i).equals(pos)){
                e++;
            }
        }
        return e;
    }
    
    public void updateValues() {

        playersNeeded.set(String.valueOf(23 - players.size()));
        if(!playersNeeded.get().equals("0"))
            moneyPP.set(String.valueOf(Integer.valueOf(moneyLeft.get()) / Integer.valueOf(playersNeeded.get())));
        else
            moneyPP.set("0");
        runs.set(initTeamRuns());
        homeRuns.set(initTeamHomeRuns());
        rbis.set(initTeamRbis());
        stolenBases.set(initTeamSb());
        battingAverage.set(initTeamBattingAverage());
        wins.set(initTeamWins());
        saves.set(initTeamSaves());
        strikeouts.set(initTeamStrikeouts());
        era.set(initTeamEra());
        whip.set(initTeamWhip());

    }

    private String initTeamRuns() {
        int sum = 0;
        for (Player player : players) {
            if (player instanceof Hitter) {
                Hitter h = (Hitter) player;
                sum += Integer.valueOf(h.getRuns());
            }
        }
        return String.valueOf(sum);
    }

    private String initTeamHomeRuns() {
        int sum = 0;
        for (Player player : players) {
            if (player instanceof Hitter) {
                Hitter h = (Hitter) player;
                sum += Integer.valueOf(h.getRuns());
            }
        }
        return String.valueOf(sum);
    }

    private String initTeamRbis() {
        int sum = 0;
        for (Player player : players) {
            if (player instanceof Hitter) {
                Hitter h = (Hitter) player;
                sum += Integer.valueOf(h.getRbis());
            }
        }
        return String.valueOf(sum);
    }

    private String initTeamSb() {
        int sum = 0;
        for (Player player : players) {
            if (player instanceof Hitter) {
                Hitter h = (Hitter) player;
                sum += Integer.valueOf(h.getStolenBases());
            }
        }
        return String.valueOf(sum);
    }

    private String initTeamBattingAverage() {
        int hi = 0;
        int ab = 0;
        for (Player player : players) {
            if (player instanceof Hitter) {
                Hitter h = (Hitter) player;
                hi += Integer.valueOf(h.getHits());
                ab += Integer.valueOf(h.getAtBat());
            }
        }

        if (hi != 0) {
            double asdf = (double) hi / ab;
            return String.valueOf((double) hi / ab).format("%.3f", asdf);
        } else {
            return "0";
        }
    }

    private String initTeamWins() {
        int sum = 0;
        for (Player player : players) {
            if (player instanceof Pitcher) {
                Pitcher p = (Pitcher) player;
                sum += Integer.valueOf(p.getWins());
            }
        }
        return String.valueOf(sum);
    }

    private String initTeamSaves() {
        int sum = 0;
        for (Player player : players) {
            if (player instanceof Pitcher) {
                Pitcher p = (Pitcher) player;
                sum += Integer.valueOf(p.getSaves());
            }
        }
        return String.valueOf(sum);
    }

    private String initTeamStrikeouts() {
        int sum = 0;
        for (Player player : players) {
            if (player instanceof Pitcher) {
                Pitcher p = (Pitcher) player;
                sum += Integer.valueOf(p.getStrikeouts());
            }
        }
        return String.valueOf(sum);
    }

    private String initTeamEra() {
        int er = 0;
        double ip = 0;
        for (Player player : players) {
            if (player instanceof Pitcher) {
                Pitcher p = (Pitcher) player;
                er += Integer.valueOf(p.getEarnedRuns());
                ip += Double.valueOf(p.getInningsPitched());
            }
        }
        
        if (er != 0) {
            double uhhh = (9 * (er / ip));
            return String.valueOf(String.valueOf(uhhh).format("%.2f", uhhh));
        } else {
            return "0";
        }
    }

    private String initTeamWhip() {
        int bb = 0;
        int h = 0;
        double ip = 0;
        for (Player player : players) {
            if (player instanceof Pitcher) {
                Pitcher p = (Pitcher) player;
                bb += Integer.valueOf(p.getBaseOnBalls());
                h += Integer.valueOf(p.getHits());
                ip += Double.valueOf(p.getInningsPitched());
            }
        }
        
        if (bb + h != 0) {
            double asdf = ((bb+h)/ ip);
            return String.valueOf((bb + h) / ip).format("%.2f", asdf);
        } else {
            return "0";
        }
    }

    //GETTERS AND SETTERS FOR THE FIELDS
    public StringProperty getPlayersNeededProperty() {
        return playersNeeded;
    }

    public String getPlayersNeeded() {
        return playersNeeded.get();
    }

    public void setPlayersNeeded(String playersNeeded) {
        this.playersNeeded.set(playersNeeded);
    }

    public StringProperty getTeamNameProperty() {
        return teamName;
    }

    public String getTeamName() {
        return teamName.get();
    }

    public void setTeamName(String teamName) {
        this.teamName.set(teamName);
    }

    public StringProperty getOwnerNameProperty() {
        return ownerName;
    }

    public String getOwnerName() {
        return ownerName.get();
    }

    /**
     * @param ownerName the ownerName to set
     */
    public void setOwnerName(String ownerName) {
        this.ownerName.set(ownerName);
    }

    public StringProperty getMoneyLeftProperty() {
        return moneyLeft;
    }

    public String getMoneyLeft() {
        return moneyLeft.get();
    }

    /**
     * @param moneyLeft the moneyLeft to set
     */
    public void setMoneyLeft(String moneyLeft) {
        this.moneyLeft.set(moneyLeft);
    }

    public StringProperty getMoneyPPProperty() {
        return moneyPP;
    }

    public String getMoneyPP() {
        return moneyPP.get();
    }

    /**
     * @param moneyPP the moneyPP to set
     */
    public void setMoneyPP(String moneyPP) {
        this.moneyPP.set(moneyPP);
    }

    public StringProperty getRunsProperty() {
        return runs;
    }

    public String getRuns() {
        return runs.get();
    }

    /**
     * @param runs the runs to set
     */
    public void setRuns(String runs) {
        this.runs.set(runs);
    }

    public StringProperty getHomeRunsProperty() {
        return homeRuns;
    }

    public String getHomeRuns() {
        return homeRuns.get();
    }

    /**
     * @param homeRuns the homeRuns to set
     */
    public void setHomeRuns(String homeRuns) {
        this.homeRuns.set(homeRuns);
    }

    public StringProperty getRbisProperty() {
        return rbis;
    }

    public String getRbis() {
        return rbis.get();
    }

    /**
     * @param rbis the rbis to set
     */
    public void setRbis(String rbis) {
        this.rbis.set(rbis);
    }

    public StringProperty getStolenBasesProperty() {
        return stolenBases;
    }

    public String getStolenBases() {
        return stolenBases.get();
    }

    /**
     * @param stolenBases the stolenBases to set
     */
    public void setStolenBases(String stolenBases) {
        this.stolenBases.set(stolenBases);
    }

    public StringProperty getBattingAverageProperty() {
        return battingAverage;
    }

    public String getBattingAverage() {
        return battingAverage.get();
    }

    /**
     * @param battingAverage the battingAverage to set
     */
    public void setBattingAverage(String battingAverage) {
        this.battingAverage.set(battingAverage);
    }

    public StringProperty getWinsProperty() {
        return wins;
    }

    public String getWins() {
        return wins.get();
    }

    /**
     * @param wins the wins to set
     */
    public void setWins(String wins) {
        this.wins.set(wins);
    }

    public StringProperty getSavesProperty() {
        return saves;
    }

    public String getSaves() {
        return saves.get();
    }

    /**
     * @param saves the saves to set
     */
    public void setSaves(String saves) {
        this.saves.set(saves);
    }

    public StringProperty getStrikeoutsProperty() {
        return strikeouts;
    }

    public String getStrikeouts() {
        return strikeouts.get();
    }

    /**
     * @param strikeouts the strikeouts to set
     */
    public void setStrikeouts(String strikeouts) {
        this.strikeouts.set(strikeouts);
    }

    public StringProperty getEraProperty() {
        return era;
    }

    public String getEra() {
        return era.get();
    }

    /**
     * @param era the era to set
     */
    public void setEra(String era) {
        this.era.set(era);
    }

    public StringProperty getWhipProperty() {
        return whip;
    }

    public String getWhip() {
        return whip.get();
    }

    /**
     * @param whip the whip to set
     */
    public void setWhip(String whip) {
        this.whip.set(whip);
    }

    public StringProperty getTotalPointsProperty() {
        return totalPoints;
    }

    public String getTotalPoints() {
        return totalPoints.get();
    }

    /**
     * @param totalPoints the totalPoints to set
     */
    public void setTotalPoints(String totalPoints) {
        this.totalPoints.set(totalPoints);
    }

    public void addPlayer(Player p) {
        players.add(p);
    }
    
    public void addTaxiPlayer(Player p){
        taxiPlayers.add(p);
    }

    //returns true if the taxi team is full
    public boolean isTaxiFull(){
        if(!taxiPlayers.isEmpty()){
            if(taxiPlayers.size() >= 8)
                return true;
            else
                return false;    
        }
        else
            return false;
    }
    //returns true if the team is completely full
    public boolean isFull(){
        if(!players.isEmpty()){
        if(players.size() >= 23){
            return true;
        }
        else{
            return false;
        }
    }
    else
    return false;
}
    public ObservableList<Player> getTaxiList(){
        return taxiPlayers;
    }
}
