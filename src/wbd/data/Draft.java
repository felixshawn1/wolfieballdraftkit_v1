package wbd.data;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import static wbd.WBD_StartupConstants.JSON_FILE_PATH_HITTERS;
import static wbd.WBD_StartupConstants.JSON_FILE_PATH_PITCHERS;
/**
 *
 * @author Shawn Felix <shawnfelixcodes.wordpress.com>
 */
public class Draft {
    
    String draftName;
    //STUFF THAT IS WITHIN A DRAFT
    ObservableList<Player> playersList;
    ObservableList<Hitter> hittersList;
    ObservableList<Pitcher> pitchersList;
    
    ObservableList<FantasyTeam> teamsList;
    
    ObservableList<Player> draftList;
    
    
    public Draft(){
        draftName = "New Draft";
        playersList = FXCollections.observableArrayList();
        hittersList = FXCollections.observableArrayList();
        pitchersList = FXCollections.observableArrayList();
        teamsList = FXCollections.observableArrayList();
        draftList = FXCollections.observableArrayList();
    }
    
    public String getDraftName(){
        return draftName;
    }
    
    public void setDraftName(String name){
        draftName = name;
    }

    public ObservableList<Player> getDraftList(){
        return draftList;
    }
    /**
     * @return the playersList
     */
    public ObservableList<Player> getPlayersList() {
        return playersList;
    }

    /**
     * @param playersList the playersList to set
     */
    public void setPlayersList(ObservableList<Player> playersList) {
        this.playersList = playersList;
    }
    
    public void addPlayerToPlayersList(Player p){
        playersList.add(p);
        Collections.sort(playersList);
    }
    
    //clears the playerlist
    public void clearPlayerList(){
        playersList.clear();
    }
    
    
    public void updateTeamValues(){
        //loop through each team and call updatevalues
        for(int i = 0; i<teamsList.size(); i++){
            teamsList.get(i).updateValues();
        }
    }
    
    /**
     * @return the playersList
     */
    public ObservableList<Hitter> getHittersList() {
        return hittersList;
    }
    /**
     * @param hittersList the playersList to set
     */
    public void setHittersList(ObservableList<Hitter> hittersList) {
        this.hittersList = hittersList;
    }
    //hittters list add and clear
    public void addPlayerToHittersList(Hitter h){
        hittersList.add(h);
        Collections.sort(playersList);
    }
    
    public void clearHittersList(){
        hittersList.clear();
    }
    
    
    
    /**
     * @return the playersList
     */
    public ObservableList<Pitcher> getPitchersList() {
        return pitchersList;
    }
    /**
     * @param pitchersList the playersList to set
     */
    public void setPitchersList(ObservableList<Pitcher> pitchersList) {
        this.pitchersList = pitchersList;
    }
    //Pitchers list add and clear
    public void addPlayerToPitchersList(Pitcher p){
        pitchersList.add(p);
        Collections.sort(playersList);
    }
    
    public void clearPitchersList(){
        hittersList.clear();
    }
    
    
    
    /**
     * @return the teamsList
     */
    public ObservableList<FantasyTeam> getTeamsList() {
        return teamsList;
    }
    /**
     * @param teamsList the playersList to set
     */
    public void setTeamsList(ObservableList<FantasyTeam> teamsList) {
        this.teamsList = teamsList;
    }
    //Teams list add and clear
    public void addPlayerToTeamsList(FantasyTeam p){
        teamsList.add(p);
    }
    
    public void clearTeamsList(){
        teamsList.clear();
    }
}
