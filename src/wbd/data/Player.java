package wbd.data;

import java.util.ArrayList;
import javafx.beans.property.ObjectProperty;
import javafx.beans.property.SimpleObjectProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;

/**
 *
 * @author Shawn Felix <shawnfelixcodes.wordpress.com>
 */
public class Player implements Comparable<Player>{
    StringProperty lastName;
    StringProperty firstName;
    StringProperty birthYear;
    StringProperty birthNation;
    StringProperty proTeam;
    StringProperty notes;
    StringProperty positions;   //qp
    
    StringProperty rw;
    StringProperty hrsv;
    StringProperty rbik;
    StringProperty sbera;
    StringProperty bawhip;
    ArrayList<String> positionsArray;
    
    StringProperty contract;
    StringProperty salary;
    StringProperty chosenPosition;
    StringProperty fantasyTeam;
    
    public Player(){
        lastName = new SimpleStringProperty();
        firstName = new SimpleStringProperty();
        birthYear = new SimpleStringProperty();
        birthNation = new SimpleStringProperty();
        proTeam = new SimpleStringProperty();      
        notes = new SimpleStringProperty(); 
        rw = new SimpleStringProperty();
        hrsv = new SimpleStringProperty();
        rbik = new SimpleStringProperty();
        sbera = new SimpleStringProperty();
        bawhip = new SimpleStringProperty();
        positions = new SimpleStringProperty();
        positionsArray = new ArrayList();
        
        contract = new SimpleStringProperty();
        salary = new SimpleStringProperty();
        chosenPosition = new SimpleStringProperty();
        fantasyTeam = new SimpleStringProperty();
    }
    public void resetPlayerInfo(){
        contract.set("");
        salary.set("");
        chosenPosition.set("");
        fantasyTeam.set("");
    }
    @Override
    public int compareTo(Player p){
       return this.lastName.get().compareTo(p.getLastName());
    }

    /**
     * @return the rw
     */
    public String getFantasyTeam() {
        return fantasyTeam.get();
    }

    public StringProperty getFantasyTeamProperty(){
        return fantasyTeam;
    }
    /**
     * @param rw the positions to set
     */
    public void setFantasyTeam(String fantasyTeam) {
        this.fantasyTeam.set(fantasyTeam);
    }
    
    /**
     * @return the rw
     */
    public String getRw() {
        return rw.get();
    }

    public StringProperty getRwProperty(){
        return rw;
    }
    /**
     * @param rw the positions to set
     */
    public void setRw(String rw) {
        this.rw.set(rw);
    }
    
    /**
     * @return the positions
     */
    public String getHrsv() {
        return hrsv.get();
    }

    public StringProperty getHrsvProperty(){
        return hrsv;
    }
    /**
     * @param hrsv the positions to set
     */
    public void setHrsv(String hrsv) {
        this.hrsv.set(hrsv);
    }
    
    /**
     * @return the positions
     */
    public String getRbik() {
        return rbik.get();
    }

    public StringProperty getRbikProperty(){
        return rbik;
    }
    /**
     * @param rbik the positions to set
     */
    public void setRbik(String rbik) {
        this.rbik.set(rbik);
    }
    
    /**
     * @return the positions
     */
    public String getSbera() {
        return sbera.get();
    }

    public StringProperty getSberaProperty(){
        return sbera;
    }
    /**
     * @param sbera the positions to set
     */
    public void setSbera(String sbera) {
        this.sbera.set(sbera);
    }
    
    /**
     * @return the positions
     */
    public String getBawhip() {
        return bawhip.get();
    }

    public StringProperty getBawhipProperty(){
        return bawhip;
    }
    /**
     * @param bawhip the positions to set
     */
    public void setBawhip(String bawhip) {
        this.bawhip.set(bawhip);
    }
    
    /**
     * @return the lastName
     */
    public String getLastName() {
        return lastName.get();
    }

    /**
     * @param lastName the lastName to set
     */
    public void setLastName(String lastName) {
        this.lastName.set(lastName);
    }
    public StringProperty lastNameProperty(){
        return lastName;
    }

    /**
     * @return the firstName
     */
    public String getFirstName() {
        return firstName.get();
    }

    public StringProperty firstNameProperty(){
        return firstName;
    }
    
    /**
     * @param firstName the firstName to set
     */
    public void setFirstName(String firstName) {
        this.firstName.set(firstName);
    }

    /**
     * @return the birthYear
     */
    public String getBirthYear() {
        return birthYear.get();
    }
    
    public StringProperty birthYearProperty(){
        return birthYear;
    }

    /**
     * @param birthYear the birthYear to set
     */
    public void setBirthYear(String birthYear) {
        this.birthYear.set(birthYear);
    }

    /**
     * @return the birthNation
     */
    public String getBirthNation() {
        return birthNation.get();
    }

    public StringProperty getBirthNationProperty(){
        return birthNation;
    }
    /**
     * @param birthNation the birthNation to set
     */
    public void setBirthNation(String birthNation) {
        this.birthNation.set(birthNation);
    }
    

    /**
     * @return the notes
     */
    public String getNotes() {
        return notes.get();
    }

    public StringProperty getNotesProperty(){
        return notes;
    }
    /**
     * @param notes the notes to set
     */
    public void setNotes(String notes) {
        this.notes.set(notes);
    }

    
    
    
    /**
     * @return the positions
     */
    public String getProTeam() {
        return proTeam.get();
    }

    public StringProperty getProTeamProperty(){
        return proTeam;
    }
    /**
     * @param positions the positions to set
     */
    public void setProTeam(String proTeam) {
        this.proTeam.set(proTeam);
    }
    
    
    /**
     * @return the positions
     */
    public String getPositions() {
        return positions.get();
    }

    public StringProperty getPositionsProperty(){
        return positions;
    }
    /**
     * @param positions the positions to set
     */
    public void setPositions(String positions) {
        this.positions.set(positions);
    }
    
    
    
    /**
     * @return the contract
     */
    public String getContract() {
        return contract.get();
    }

    public StringProperty getContractProperty(){
        return contract;
    }
    /**
     * @param positions the positions to set
     */
    public void setContract(String contract) {
        this.contract.set(contract);
    }
    /**
     * @return the salary
     */
    public String getSalary() {
        return salary.get();
    }

    public StringProperty getSalaryProperty(){
        return salary;
    }
    /**
     * @param positions the positions to set
     */
    public void setSalary(String salary) {
        this.salary.set(salary);
    }
    
    /**
     * @return the chosenPosition
     */
    public String getChosenPosition() {
        return chosenPosition.get();
    }

    public StringProperty getChosenPositionProperty(){
        return chosenPosition;
    }
    /**
     * @param positions the positions to set
     */
    public void setChosenPosition(String chosenPosition) {
        this.chosenPosition.set(chosenPosition);
    }
}
