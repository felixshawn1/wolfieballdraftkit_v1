package wbd.error;

import wbd.error.ErrorHandler;
import wbd.gui.WBD_GUI;
import wbd.gui.MessageDialog;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Locale;
import javafx.application.Application;
import javafx.stage.Stage;
import properties_manager.PropertiesManager;
import xml_utilities.InvalidXMLFileFormatException;

/**
 *
 * @author Shawn Felix <shawnfelixcodes.wordpress.com>
 */
public class ErrorHandler {
    
    static ErrorHandler singleton;
    
    //PROVIDE MESSAGE FEEDBACK
    MessageDialog messageDialog;
    
    //PROPERTIES MANAGER GIVES THE TEXT TO DISPLAY
    PropertiesManager properties;
    
    private ErrorHandler(){
        //KEEP TRACK OF WHEETHER WE NEED TO CONSTRUCT SINGLETON
        singleton = null;
        
        //GET THE SINGLETON ONCE
        properties = PropertiesManager.getPropertiesManager();
    }
    
    public static ErrorHandler getErrorHandler(){
        
        if(singleton == null)
            singleton = new ErrorHandler();
        
        //ALWAYS RETURN
        return singleton;
    }
    
    public void handleNewDraftError(){
        
    }
    public void handlePropertiesFileError(){
        
    }
    
}
