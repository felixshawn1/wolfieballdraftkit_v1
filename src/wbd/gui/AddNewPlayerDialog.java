package wbd.gui;

import java.util.ArrayList;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.CheckBox;
import javafx.scene.control.ComboBox;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.layout.FlowPane;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.VBox;
import javafx.stage.Modality;
import javafx.stage.Stage;
import wbd.data.Hitter;
import wbd.data.Pitcher;
import wbd.data.Player;

/**
 *
 * @author Shawn Felix <shawnfelixcodes.wordpress.com>
 */
public class AddNewPlayerDialog extends Stage{
    VBox mainPane;
    Scene mainScene;
    Label headingLabel;
    ArrayList<String> allTeams;
    
    GridPane controlsPane;
    Label firstNameLabel;
    TextField firstNameText;
    Label lastNameLabel;
    TextField lastNameText;
    Label proTeamLabel;
    ComboBox proTeamComboBox;
    
    FlowPane checkBoxPane;
    CheckBox catcher;
    Label cLabel;
    CheckBox firstBase;
    Label fbLabel;
    CheckBox thirdBase;
    Label tbLabel;
    CheckBox secondBase;
    Label sbLabel;
    CheckBox shortstop;
    Label ssLabel;
    CheckBox outfield;
    Label ofLabel;
    CheckBox pitcher;
    Label pLabel;
    
    Player newPlayer;
    
    FlowPane buttonPane;
    Button completeButton;
    Button cancelButton;
    
    /**
     * Initializes this dialog so that it can be used repeatedly
     * for all kinds of messages.
     * 
     * @param owner The owner stage of this modal dialog.
     * 
     * @param closeButtonText Text to appear on the close button.
     */
    public AddNewPlayerDialog(Stage owner) {
        this.setTitle("Add a New Player");
        allTeams = new ArrayList();
        
        mainPane = new VBox();
        // MAKE IT MODAL
        initModality(Modality.WINDOW_MODAL);
        initOwner(owner);
        
        
    }
 
    public String getCheckBoxPositions(){
        String output = "";
        if(catcher.selectedProperty().get() == true)
            output += "C_";
        if(firstBase.selectedProperty().get() == true)
            output += "1B_";
        if(thirdBase.selectedProperty().get() == true)
            output += "3B_";
        if(secondBase.selectedProperty().get() == true)
            output += "2B_";
        if(shortstop.selectedProperty().get() == true)
            output += "SS_";
        if(outfield.selectedProperty().get() == true)
            output += "OF_";
        if(pitcher.selectedProperty().get() == true)
            output += "P_";
        
        if(output.endsWith("_"))
        output = output.substring(0, output.length() -1);
            
        return output;
    }
    
    private void initCheckBoxListeners(){
        pitcher.selectedProperty().addListener(new ChangeListener<Boolean>(){
            @Override
            public void changed(ObservableValue<? extends Boolean> observable, Boolean oldValue, Boolean newValue) {
                if(pitcher.selectedProperty().get()){
                    catcher.setSelected(false);
                    firstBase.setSelected(false);
                    thirdBase.setSelected(false);
                    secondBase.setSelected(false);
                    shortstop.setSelected(false);
                    outfield.setSelected(false);
                }
            }
        });
        catcher.selectedProperty().addListener(new ChangeListener<Boolean>(){
            @Override
            public void changed(ObservableValue<? extends Boolean> observable, Boolean oldValue, Boolean newValue) {
                if(catcher.selectedProperty().get()){
                    pitcher.setSelected(false);
                }
        
            }
        });
        firstBase.selectedProperty().addListener(new ChangeListener<Boolean>(){
            @Override
            public void changed(ObservableValue<? extends Boolean> observable, Boolean oldValue, Boolean newValue) {
                if(firstBase.selectedProperty().get()){
                    pitcher.setSelected(false);
                }
        
            }
        });
        thirdBase.selectedProperty().addListener(new ChangeListener<Boolean>(){
            @Override
            public void changed(ObservableValue<? extends Boolean> observable, Boolean oldValue, Boolean newValue) {
                if(thirdBase.selectedProperty().get()){
                    pitcher.setSelected(false);
                }
        
            }
        });
        secondBase.selectedProperty().addListener(new ChangeListener<Boolean>(){
            @Override
            public void changed(ObservableValue<? extends Boolean> observable, Boolean oldValue, Boolean newValue) {
                if(secondBase.selectedProperty().get()){
                    pitcher.setSelected(false);
                }
        
            }
        });
        shortstop.selectedProperty().addListener(new ChangeListener<Boolean>(){
            @Override
            public void changed(ObservableValue<? extends Boolean> observable, Boolean oldValue, Boolean newValue) {
                if(shortstop.selectedProperty().get()){
                    pitcher.setSelected(false);
                }
        
            }
        });
        outfield.selectedProperty().addListener(new ChangeListener<Boolean>(){
            @Override
            public void changed(ObservableValue<? extends Boolean> observable, Boolean oldValue, Boolean newValue) {
                if(outfield.selectedProperty().get()){
                    pitcher.setSelected(false);
                }
        
            }
        });
        
    }
    public ArrayList<String> populateArrayList(ArrayList<String> list){
        list.add("AZ");
        list.add("ATL");
        list.add("CHC");
        list.add("CIN");
        list.add("COL");
        list.add("LAD");
        list.add("MIA");
        list.add("MIL");
        list.add("NYM");
        list.add("PHI");
        list.add("PIT");
        list.add("SD");
        list.add("SF");
        list.add("STL");
        list.add("TOR");
        list.add("WSH");
        
        return list;
    }
    /**
     * This method loads a custom message into the label and
     * then pops open the dialog.
     * 
     * @param message Message to appear inside the dialog.
     */
    public void show(WBD_GUI gui) {
        
        
        headingLabel = new Label("Player Details");        
        
        
        controlsPane = new GridPane();
        firstNameLabel = new Label("First Name:");
        firstNameText = new TextField();
        lastNameLabel = new Label("Last Name:");
        lastNameText = new TextField();
        proTeamLabel = new Label("Pro Team:");
        proTeamComboBox = new ComboBox();
        
        controlsPane.add(firstNameLabel, 0, 0);
        controlsPane.add(firstNameText, 1, 0);
        controlsPane.add(lastNameLabel, 0, 1);
        controlsPane.add(lastNameText, 1, 1);
        controlsPane.add(proTeamLabel, 0, 2);
        controlsPane.add(proTeamComboBox, 1, 2);
        
        allTeams = populateArrayList(allTeams);
        //loop through the array
        allTeams.stream().forEach((allTeam) -> {
            proTeamComboBox.getItems().add(allTeam);
        });
        
        
        checkBoxPane = new FlowPane();
        
        catcher = new CheckBox();
        firstBase = new CheckBox();
        thirdBase = new CheckBox();
        secondBase = new CheckBox();
        shortstop = new CheckBox();
        outfield = new CheckBox();
        pitcher = new CheckBox();
        
        cLabel = new Label("C");
        fbLabel = new Label("1B");
        tbLabel = new Label("3B");
        sbLabel = new Label("2B");
        ssLabel = new Label("SS");
        ofLabel = new Label("OF");
        pLabel = new Label("P");
        
        checkBoxPane.getChildren().add(catcher);
        checkBoxPane.getChildren().add(cLabel);
        checkBoxPane.getChildren().add(firstBase);
        checkBoxPane.getChildren().add(fbLabel);
        checkBoxPane.getChildren().add(thirdBase);
        checkBoxPane.getChildren().add(tbLabel);
        checkBoxPane.getChildren().add(secondBase);
        checkBoxPane.getChildren().add(sbLabel);
        checkBoxPane.getChildren().add(shortstop);
        checkBoxPane.getChildren().add(ssLabel);
        checkBoxPane.getChildren().add(outfield);
        checkBoxPane.getChildren().add(ofLabel);
        checkBoxPane.getChildren().add(pitcher);
        checkBoxPane.getChildren().add(pLabel);
        
        //set up checkbox listeners
        initCheckBoxListeners();
        
        
        buttonPane = new FlowPane();
        //COMPLETE BUTTON
        completeButton = new Button("Complete");
        completeButton.setOnAction(e->{
            
            if(pitcher.selectedProperty().get()){
                newPlayer = new Pitcher();
                newPlayer.setFirstName(firstNameText.getText());
                newPlayer.setLastName(lastNameText.getText());
                newPlayer.setPositions(getCheckBoxPositions());
                newPlayer.setProTeam(proTeamComboBox.getValue().toString());
            }
            else{
                newPlayer = new Hitter();
                newPlayer.setFirstName(firstNameText.getText());
                newPlayer.setLastName(lastNameText.getText());
                newPlayer.setProTeam(proTeamComboBox.getValue().toString());
                newPlayer.setPositions(getCheckBoxPositions());
            }
            System.out.println(newPlayer.getFirstName() + newPlayer.getLastName() + newPlayer.getProTeam());
            gui.getDataManager().getDraft().getPlayersList().add(newPlayer);
            //table needs to be sorted at this point
            
            AddNewPlayerDialog.this.close();
        });
        
        // CANCEL BUTTON
        cancelButton = new Button("Cancel");
        cancelButton.setOnAction(e->{ AddNewPlayerDialog.this.close(); });
        buttonPane.getChildren().add(completeButton);
        buttonPane.getChildren().add(cancelButton);
        
        // WE'LL PUT EVERYTHING HERE
        mainPane.getChildren().add(headingLabel);
        mainPane.getChildren().add(controlsPane);
        mainPane.getChildren().add(checkBoxPane);
        mainPane.getChildren().add(buttonPane);
        mainPane.setAlignment(Pos.CENTER);
        
        
        // MAKE IT LOOK NICE
        mainPane.setPadding(new Insets(10, 20, 20, 20));
        mainPane.setSpacing(10);

        // AND PUT IT IN THE WINDOW
        mainScene = new Scene(mainPane);
        this.setScene(mainScene);
        
        this.showAndWait();
    }
}
