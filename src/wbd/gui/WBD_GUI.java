package wbd.gui;

import wbd.data.DraftDataManager;
import wbd.data.DraftDataView;
import wbd.data.Draft;
import wbd.file.DraftFileManager;
import wbd.file.DraftSiteExporter;
import wbd.controller.FileController;
import wbd.WBD_PropertyType;
import static wbd.WBD_StartupConstants.*;
import wbd.data.Player;

import java.io.IOException;
import java.net.URL;
import java.time.DayOfWeek;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;
import java.util.Iterator;
import javafx.application.Platform;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.collections.ListChangeListener;
import javafx.collections.ObservableList;
import javafx.concurrent.Task;
import javafx.geometry.Rectangle2D;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.CheckBox;
import javafx.scene.control.ComboBox;
import javafx.scene.control.DatePicker;
import javafx.scene.control.Label;
import javafx.scene.control.RadioButton;
import javafx.scene.control.ScrollPane;
import javafx.scene.control.SplitPane;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.control.ToggleGroup;
import javafx.scene.control.Tooltip;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.FlowPane;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Pane;
import javafx.scene.layout.VBox;
import javafx.stage.Screen;
import javafx.stage.Stage;
import properties_manager.PropertiesManager;
import wbd.controller.DraftEditController;
import wbd.data.FantasyTeam;
import wbd.gui.screens.AvailablePlayersScreen;
import wbd.gui.screens.DraftSummaryScreen;
import wbd.gui.screens.FantasyStandingsScreen;
import wbd.gui.screens.FantasyTeamsScreen;
import wbd.gui.screens.MlbTeamsScreen;

/**
 *
 * @author Shawn Felix <shawnfelixcodes.wordpress.com>
 */
public class WBD_GUI implements DraftDataView {

    //STYLESHEET STRING CONSTANTS
    static final String PRIMARY_STYLE_SHEET = PATH_CSS + "wbd_style.css";
    static final String CLASS_BORDERED_PANE = "bordered_pane";
    static final String CLASS_SUBJECT_PANE = "subject_pane";
    static final String CLASS_HEADING_LABEL = "heading_label";
    static final String CLASS_SUBHEADING_LABEL = "subheading_label";
    static final String CLASS_PROMPT_LABEL = "prompt_label";
    static final String EMPTY_TEXT = "";
    static final int LARGE_TEXT_FIELD_LENGTH = 20;
    static final int SMALL_TEXT_FIELD_LENGTH = 5;

    //MANAGES APPLICATION'S DATA
    DraftDataManager dataManager;

    //MANAGE DRAFT FILE I/O
    DraftFileManager draftFileManager;

    //MANAGE SITE EXPORTING
    DraftSiteExporter siteExporter;

    //MANAGES FILE INTERATIONS
    FileController fileController;

    //HANDLES INTERACTIONS WITH DRAFT INFO CONTROLS
    DraftEditController draftController;

    //MAIN APPLICATION WINDOW
    Stage primaryStage;

    //MAIN SCENE
    Scene primaryScene;

    //OVERAL ORGANIZATION OF THE MAIN SCENE
    BorderPane wbdPane;

    //TOP TOOLBAR OF THE MAIN SCENE
    FlowPane fileToolbarPane;

    //TOOLBAR BUTTONS
    Button newDraftButton;
    Button loadDraftButton;
    Button saveDraftButton;
    Button exportSiteButton;
    Button exitButton;

    //MAIN WORKSPACE COMPONENT
    BorderPane workspacePane;
    // USED TO CHECK IF A WORKSPACE HAS BEEN ACTIVATED
    boolean workspaceActivated;

    //PUT THE WORKSPACE INTO A SCROLL PANE
    ScrollPane workspaceScrollPane;

    //PANE FOR THE SCREENS
    public BorderPane allScreensPane;
    public VBox fantasyTeamsPane;
    public VBox availablePlayersPane;
    public VBox fantasyStandingsPane;
    public VBox draftSummaryPane;
    public VBox mlbTeamsPane;

    private FantasyTeamsScreen ftScreen;
    private AvailablePlayersScreen apScreen;
    private FantasyStandingsScreen fsScreen;
    private DraftSummaryScreen dsScreen;
    private MlbTeamsScreen mScreen;

    //mlbTeamsPane
    Label mlbTeamsHeaderLabel;

    //HOLDS THE BOTTOM BUTTONS
    FlowPane screenButtonsToolbar;

    //SCREEN SWITCHING BUTTONS
    Button fantasyTeamsButton;
    Button availablePlayersButton;
    Button fantasyStandingsButton;
    Button draftSummaryButton;
    Button mlbTeamsButton;

    boolean draftPaused;
    MessageDialog messageDialog;
    NewFantasyTeamDialog newFantasyTeamDialog;

    EditPlayerDialog editPlayerDialog;
    YesNoCancelDialog yesNoCancelDialog;

    /**
     * Constructor for making the GUi but it does not initialize it call initGui
     * to initialize it
     *
     * @param initPrimaryStage Window inside which the GUI will be displayed
     */
    public WBD_GUI(Stage initPrimaryStage) {
        primaryStage = initPrimaryStage;
    }

    /**
     * Accessor method for the data manager for the instance of the GUI
     *
     * @return The dataManager used by this GUI
     */
    public DraftDataManager getDataManager() {
        return dataManager;
    }

    /**
     * Accessor method for the file controller for the instance of the GUI
     *
     * @return The fileController used by the GUI
     */
    public FileController getFileController() {
        return fileController;
    }

    /**
     * Accessor method for the draft file manager.
     *
     * @return The DraftFileManager used by this UI.
     */
    public DraftFileManager getCourseFileManager() {
        return draftFileManager;
    }

    /**
     * Accessor method for the site exporter.
     *
     * @return The DraftSiteExporter used by this UI.
     */
    public DraftSiteExporter getSiteExporter() {
        return siteExporter;
    }

    /**
     * Accessor method for the window (i.e. stage).
     *
     * @return The window (i.e. Stage) used by this UI.
     */
    public Stage getWindow() {
        return primaryStage;
    }

    /**
     * Accessor method for the messageDialog of the instance of the GUI
     *
     * @return The messageDialog of the GUI
     */
    public MessageDialog getMessageDialog() {
        return messageDialog;
    }

    /**
     * Accessor method for the yesNoCancelDialog for the instance of the GUI
     *
     * @return The yesNoCancelDialog of the GUI
     */
    public YesNoCancelDialog getYesNoCancelDialog() {
        return yesNoCancelDialog;
    }

    /**
     * Mutator method for the data manager.
     *
     * @param initDataManager The DraftDataManager to be used by this UI.
     */
    public void setDataManager(DraftDataManager initDataManager) {
        dataManager = initDataManager;
    }

    /**
     * This method initializes the user interface for use.
     *
     * @param windowTitle The text to appear in the window's title bar
     * @throws IOException Thrown if any init files fail to load
     */
    public void initGUI(String windowTitle, ObservableList<Player> list) throws IOException {
        //INITIALIZED THE DIALOGS
        initDialogs();

        //INITIALIZES FILE TOOLBAR WITH TOP BUTTONS SAVE, LOAD, ETC
        initFileToolbar();

        initScreenButtonsToolbar();

        //INITIALIZES THE WORKSPACE CONTROLS
        initWorkspace(list);

        //INITIALIZE THE EVENT HANDLERS
        initEventHandlers();

        //INITIALIZE THE WINDOW
        initWindow(windowTitle);

        dsScreen.getDraftTable().setItems(dataManager.getDraft().getDraftList());
        //DONT PUT THE WORKSPACE INTO THE WINDOW UNTIL USER HITS
        //NEW DRAFT OR LOADS DRAFT
        workspaceActivated = false;

    }

    /**
     * This functions loads all the values currently in the user interface into
     * the draft argument
     *
     * @param draft the draft to be updated using the data from the UI
     */
    public void updateDraftInfo(Draft draft) {
        draft.setDraftName(getFtScreen().draftNameBox.getText());

    }

    /**
     * This method is used to activate/deactivate toolbar buttons when they can
     * and cannot be used so as to provide foolproof design.
     *
     * @param saved Describes whether the loaded Course has been saved or not.
     */
    public void updateToolbarControls(boolean saved) {
        // THIS TOGGLES WITH WHETHER THE CURRENT COURSE
        // HAS BEEN SAVED OR NOT
        saveDraftButton.setDisable(saved);

        // ALL THE OTHER BUTTONS ARE ALWAYS ENABLED
        // ONCE EDITING THAT FIRST COURSE BEGINS
        loadDraftButton.setDisable(false);
        exportSiteButton.setDisable(false);

        // NOTE THAT THE NEW, LOAD, AND EXIT BUTTONS
        // ARE NEVER DISABLED SO WE NEVER HAVE TO TOUCH THEM
    }

    /**
     * When called this function puts the workspace into the window, revealing
     * the controls for editing a Course.
     */
    public void activateWorkspace() {
        if (!workspaceActivated) {
            //Put the workspace in the GUI
            wbdPane.setCenter(workspaceScrollPane);
            workspaceActivated = true;
        }
    }

    /**
     * This function takes all of the data out of the draftToReload argument and
     * loads its values into the user interface controls.
     *
     * @param draftToReload The Draft whose data we'll load into the GUI.
     */
    @Override
    public void reloadDraft(Draft draftToReload) {
        if (!workspaceActivated) {
            activateWorkspace();
        }

        //draftController.enable(false);
    }

    public void setDraftFileManager(DraftFileManager initDraftFileManager) {
        draftFileManager = initDraftFileManager;

    }

    /**
     * *************************************************************************
     */
    /* BELOW ARE ALL THE PRIVATE HELPER METHODS WE USE FOR INITIALIZING OUR GUI */
    /**
     * *************************************************************************
     */
    private void initDialogs() {
        messageDialog = new MessageDialog(primaryStage, CLOSE_BUTTON_LABEL);
        newFantasyTeamDialog = new NewFantasyTeamDialog(primaryStage, this);

        editPlayerDialog = new EditPlayerDialog(primaryStage, this);
        yesNoCancelDialog = new YesNoCancelDialog(primaryStage);
    }

    private void initFileToolbar() {
        fileToolbarPane = new FlowPane();

        //INITIALIZE ALL THE BUTTONS FOR THE FILE TOOLBAR
        newDraftButton = initChildButton(fileToolbarPane, WBD_PropertyType.NEW_COURSE_ICON, WBD_PropertyType.NEW_COURSE_TOOLTIP, false);
        loadDraftButton = initChildButton(fileToolbarPane, WBD_PropertyType.LOAD_COURSE_ICON, WBD_PropertyType.LOAD_COURSE_TOOLTIP, false);
        saveDraftButton = initChildButton(fileToolbarPane, WBD_PropertyType.SAVE_COURSE_ICON, WBD_PropertyType.SAVE_COURSE_TOOLTIP, true);
        exportSiteButton = initChildButton(fileToolbarPane, WBD_PropertyType.EXPORT_PAGE_ICON, WBD_PropertyType.EXPORT_PAGE_TOOLTIP, true);
        exitButton = initChildButton(fileToolbarPane, WBD_PropertyType.EXIT_ICON, WBD_PropertyType.EXIT_TOOLTIP, false);

    }

    private void initScreenButtonsToolbar() {
        screenButtonsToolbar = new FlowPane();
        screenButtonsToolbar.setVisible(false);

        //INITIALIZE THE BUTTONS
        fantasyTeamsButton = initChildButton(screenButtonsToolbar, WBD_PropertyType.FANTASY_TEAMS_ICON, WBD_PropertyType.FANTASY_TEAMS_TOOLTIP, false);
        availablePlayersButton = initChildButton(screenButtonsToolbar, WBD_PropertyType.PLAYERS_SCREEN_ICON, WBD_PropertyType.PLAYERS_SCREEN_TOOLTIP, false);
        fantasyStandingsButton = initChildButton(screenButtonsToolbar, WBD_PropertyType.FANTASY_STANDINGS_ICON, WBD_PropertyType.FANTASY_STANDINGS_TOOLTIP, false);
        draftSummaryButton = initChildButton(screenButtonsToolbar, WBD_PropertyType.DRAFT_SCREEN_ICON, WBD_PropertyType.DRAFT_SCREEN_TOOLTIP, false);
        mlbTeamsButton = initChildButton(screenButtonsToolbar, WBD_PropertyType.MLB_TEAMS_ICON, WBD_PropertyType.MLB_TEAMS_TOOLTIP, false);

    }

    /**
     * Initializes all the vboxes for each screen and sets fantasy teams as the
     * visible one.
     */
    private void initScreens(ObservableList<Player> list) {

        allScreensPane = new BorderPane();

        fantasyTeamsPane = new VBox();
        availablePlayersPane = new VBox();
        fantasyStandingsPane = new VBox();
        draftSummaryPane = new VBox();
        mlbTeamsPane = new VBox();

        dataManager.getDraft().setPlayersList(list);

        //inits the screen
        ftScreen = new FantasyTeamsScreen(dataManager, fantasyTeamsPane, this);
        apScreen = new AvailablePlayersScreen(dataManager, availablePlayersPane, dataManager.getDraft().getPlayersList());
        fsScreen = new FantasyStandingsScreen(dataManager, fantasyStandingsPane, this);
        dsScreen = new DraftSummaryScreen(dataManager, draftSummaryPane);
        mScreen = new MlbTeamsScreen(dataManager, mlbTeamsPane, this);

        //make only the fantasyTeams screen visible upon startup
        allScreensPane.setCenter(fantasyTeamsPane);

    }

    private void initRadioButton(RadioButton button, FlowPane pane, ToggleGroup group, String text, boolean selected) {
        button = new RadioButton();
        button.setToggleGroup(group);
        button.setSelected(selected);
        button.setText(text);
        pane.getChildren().add(button);
    }

    /**
     * Initializes the overall frame
     *
     * @param windowTitle Title of the window
     */
    private void initWindow(String windowTitle) {
        //SET THE TITLE OF THE WINDOW
        primaryStage.setTitle(windowTitle);

        //GET THE SCREEN SIZE
        Screen screen = Screen.getPrimary();
        Rectangle2D bounds = screen.getVisualBounds();

        // AND USE IT TO SIZE THE WINDOW
        primaryStage.setX(bounds.getMinX());
        primaryStage.setY(bounds.getMinY());
        primaryStage.setWidth(bounds.getWidth());
        primaryStage.setHeight(bounds.getHeight());

        // THIS PANE GOES IN THE SCENE TO ORGANIZE ALL THE WORKSPACE STUFF
        wbdPane = new BorderPane();
        wbdPane.setTop(fileToolbarPane);
        wbdPane.setBottom(screenButtonsToolbar);

        primaryScene = new Scene(wbdPane);

        // NOW TIE THE SCENE TO THE WINDOW, SELECT THE STYLESHEET
        // WE'LL USE TO STYLIZE OUR GUI CONTROLS, AND OPEN THE WINDOW
        //@todo
        primaryScene.getStylesheets().add(PRIMARY_STYLE_SHEET);
        primaryStage.setScene(primaryScene);
        primaryStage.show();

    }

    /**
     * This method initializes the workspace
     */
    private void initWorkspace(ObservableList<Player> list) throws IOException {
        //@todo

        //INITIALIZE MAIN WORKSPACE AREA
        workspacePane = new BorderPane();

        //INITIALIZE ALL THE VBOXES
        initScreens(list);

        //PUT VBOX IN CENTER AND BUTTONS ON THE BOTTOM
        workspacePane.setCenter(allScreensPane);
        workspacePane.getStyleClass().add(CLASS_BORDERED_PANE);
        //put it in the workspace scroll pane
        workspaceScrollPane = new ScrollPane();
        workspaceScrollPane.setContent(workspacePane);
        workspaceScrollPane.setFitToWidth(true);

        workspaceActivated = false;

    }

    private void registerTextFieldController(TextField textField) {
        textField.textProperty().addListener((observable, oldValue, newValue) -> {
            //draftController.handleDraftChangeRequest(this);

            try {
                //UPDATE THE DRAFT
                this.updateDraftInfo(this.getDataManager().getDraft());

                //THE DRAFT IS NOW CHANGED
                this.getFileController().markAsEdited(this);

            } catch (Exception e) {
                System.out.println("PROBLEM HANDLING THE DRAFT CHANGE REQUEST");
            }

        });
    }

    private void registerTeamsListener(ObservableList<FantasyTeam> list) {
        list.addListener(new ListChangeListener() {
            @Override
            public void onChanged(ListChangeListener.Change c) {
                //fill up the fantasy teams screen combo box
                ftScreen.getComboBox().getItems().clear();
                for (int i = 0; i < list.size(); i++) {
                    list.get(i).getTeamName();
                    ftScreen.getComboBox().getItems().add(list.get(i).getTeamName());
                }

            }
        });
    }

    private void registerAvailablePlayersListener(ObservableList<Player> list) {
        list.addListener(new ListChangeListener() {
            @Override
            public void onChanged(ListChangeListener.Change c) {
                dataManager.getDraft().updateTeamValues();
                //fsScreen.getTeamsTable().getColumns().get(0).setVisible(false);
                //fsScreen.getTeamsTable().getColumns().get(0).setVisible(true);
            }
        });
    }

    /**
     * This method initializes a label and sets its stylesheet class
     *
     */
    private Label initLabel(WBD_PropertyType labelProperty, String styleClass) {
        PropertiesManager props = PropertiesManager.getPropertiesManager();
        String labelText = props.getProperty(labelProperty);
        Label label = new Label(labelText);
        label.getStyleClass().add(styleClass);
        return label;
    }

    private Label initGridLabel(GridPane container, String styleClass, WBD_PropertyType labelProperty, int col, int row, int colSpan, int rowSpan) {
        Label label = initLabel(labelProperty, styleClass);
        container.add(label, col, row, colSpan, rowSpan);
        return label;
    }

    /**
     * this method initializes a label and puts it in a pane
     *
     * @param container
     * @param labelProperty
     * @param styleClass
     * @return
     */
    private Label initChildLabel(Pane container, WBD_PropertyType labelProperty, String styleClass) {
        Label label = initLabel(labelProperty, styleClass);
        container.getChildren().add(label);
        return label;
    }

    private Button initButton(WBD_PropertyType icon, WBD_PropertyType tooltip, boolean disabled) {
        PropertiesManager props = PropertiesManager.getPropertiesManager();
        String imagePath = "file:" + PATH_IMAGES + props.getProperty(icon.toString());
        Image buttonImage = new Image(imagePath);
        Button button = new Button();
        button.setDisable(disabled);
        button.setGraphic(new ImageView(buttonImage));
        Tooltip buttonTooltip = new Tooltip(props.getProperty(tooltip.toString()));
        button.setTooltip(buttonTooltip);
        return button;
    }

    /**
     * This method initializes a button and adds it to the container in a
     * toolbar
     *
     * @param toolbar The toolbar that the button is to be added to
     * @param icon The image that the button shall use
     * @param tooltip The tooltip that the button shall have
     * @param disabled Is the button
     * @return
     */
    private Button initChildButton(Pane toolbar, WBD_PropertyType icon, WBD_PropertyType tooltip, boolean disabled) {
        PropertiesManager props = PropertiesManager.getPropertiesManager();
        String imagePath = "file:" + PATH_IMAGES + props.getProperty(icon.toString());
        Image buttonImage = new Image(imagePath);
        Button button = new Button();
        button.setDisable(disabled);
        button.setGraphic(new ImageView(buttonImage));
        Tooltip buttonTooltip = new Tooltip(props.getProperty(tooltip.toString()));
        button.setTooltip(buttonTooltip);
        toolbar.getChildren().add(button);
        return button;
    }

    private Button initGridButton(GridPane container, WBD_PropertyType icon, WBD_PropertyType tooltip, boolean disabled, int col, int row, int colSpan, int rowSpan) {
        Button button = initButton(icon, tooltip, disabled);
        container.add(button, col, row, colSpan, rowSpan);
        return button;
    }

    /**
     * This method initializes all the event handlers
     */
    private void initEventHandlers() throws IOException {

        //FILE CONTROLS
        fileController = new FileController(messageDialog, yesNoCancelDialog, newFantasyTeamDialog, draftFileManager);
        newDraftButton.setOnAction(e -> {
            fileController.handleNewDraftRequest(this);
            screenButtonsToolbar.setVisible(true);
        });
        loadDraftButton.setOnAction(e -> {
            fileController.handleLoadDraftRequest(this);
            screenButtonsToolbar.setVisible(true);
        });
        saveDraftButton.setOnAction(e -> {
            fileController.handleSaveDraftRequest(this, dataManager.getDraft());
        });/*
         exportSiteButton.setOnAction(e -> {
         fileController.handleExportDraftRequest(this);
         });*/

        exitButton.setOnAction(e -> {
            fileController.handleExitRequest(this);
        });

        registerTextFieldController(getFtScreen().getDraftNameTextField());

        registerTeamsListener(dataManager.getDraft().getTeamsList());

        registerAvailablePlayersListener(dataManager.getDraft().getPlayersList());

        availablePlayersButton.setOnAction(e -> {
            fileController.handleAvailablePlayersRequest(this);
        });
        fantasyStandingsButton.setOnAction(e -> {
            fileController.handleFantasyStandingsRequest(this);
        });
        fantasyTeamsButton.setOnAction(e -> {
            fileController.handleFantasyTeamsRequest(this);
        });
        draftSummaryButton.setOnAction(e -> {
            fileController.handleDraftSummaryRequest(this);
        });
        mlbTeamsButton.setOnAction(e -> {
            fileController.handleMlbTeamsRequest(this);
        });

        getApScreen().getPlayerTableView().setOnMouseClicked(e -> {
            if (e.getClickCount() == 2) {
                Player p = getApScreen().getPlayerTableView().getSelectionModel().getSelectedItem();
                editPlayerDialog.show(p);
                editPlayerDialog.reset();
            }
        });
        getApScreen().getAddPlayerButton().setOnAction(e -> {
            AddNewPlayerDialog addNewPlayerDialog = new AddNewPlayerDialog(primaryStage);
            addNewPlayerDialog.show(this);
        });

        getFtScreen().getEditPlayerContractButton().setOnAction(e -> {
            if (ftScreen.getComboBox().getSelectionModel().getSelectedItem() != null) {
                int g = -1;
                //find the location of the the team in the list
                for (int i = 0; i < dataManager.getDraft().getTeamsList().size(); i++) {
                    if (ftScreen.getComboBox().getValue().toString().equals(dataManager.getDraft().getTeamsList().get(i).getTeamName())) {
                        g = i;
                    }
                }
                if (!dataManager.getDraft().getTeamsList().get(g).getPlayersList().isEmpty()) {
                    EditContractDialog contractDialog = new EditContractDialog(primaryStage, ftScreen.getTable().getSelectionModel().getSelectedItem(), this);
                    contractDialog.show();

                }
            }
        });

        getFtScreen().getRemoveTaxiButton().setOnAction(e -> {
            if (ftScreen.getComboBox().getSelectionModel().getSelectedItem() != null) {
                int g = 0;
                //find hte location of the team in the list
                for (int i = 0; i < dataManager.getDraft().getTeamsList().size(); i++) {
                    if (ftScreen.getComboBox().getValue().toString().equals(dataManager.getDraft().getTeamsList().get(i).getTeamName())) {
                        g = i;
                    }
                }
                //if the current selected team list is not empty
                if (!dataManager.getDraft().getTeamsList().get(g).getTaxiList().isEmpty()) {
                    int playerSalary = Integer.valueOf(ftScreen.getTaxiTable().getSelectionModel().getSelectedItem().getSalary());
                    int teamMoneyLeft = Integer.valueOf(dataManager.getDraft().getTeamsList().get(g).getMoneyLeft());

                    ftScreen.getTaxiTable().getSelectionModel().getSelectedItem().resetPlayerInfo();

                    //update the team salary
                    dataManager.getDraft().getTeamsList().get(g).setMoneyLeft(String.valueOf(teamMoneyLeft + playerSalary));

                    //add the team to available players list
                    dataManager.getDraft().getPlayersList().add(ftScreen.getTaxiTable().getSelectionModel().getSelectedItem());

                    //remove the player from the list
                    dataManager.getDraft().getTeamsList().get(g).getTaxiList().remove(ftScreen.getTaxiTable().getSelectionModel().getSelectedItem());

                    //remove the player from the draft list
                    dataManager.getDraft().getDraftList().remove(ftScreen.getTaxiTable().getSelectionModel().getSelectedItem());

                }
            }

        });

        getFtScreen().getRemovePlayerButton().setOnAction(e -> {
            if (ftScreen.getComboBox().getSelectionModel().getSelectedItem() != null) {
                int g = -1;
                //find the location of the the team in the list
                for (int i = 0; i < dataManager.getDraft().getTeamsList().size(); i++) {
                    if (ftScreen.getComboBox().getValue().toString().equals(dataManager.getDraft().getTeamsList().get(i).getTeamName())) {
                        g = i;
                    }
                }
                //if the current selected team list is not empty
                if (!dataManager.getDraft().getTeamsList().get(g).getPlayersList().isEmpty()) {
                    int playerSalary = Integer.valueOf(ftScreen.getTable().getSelectionModel().getSelectedItem().getSalary());
                    int teamMoneyLeft = Integer.valueOf(dataManager.getDraft().getTeamsList().get(g).getMoneyLeft());

                    ftScreen.getTable().getSelectionModel().getSelectedItem().resetPlayerInfo();

                    dataManager.getDraft().getTeamsList().get(g)
                            .setPlayersNeeded(String.valueOf(Integer.valueOf(dataManager.getDraft().getTeamsList().get(g).getMoneyLeft()) + 1));
                    //update the team salary
                    dataManager.getDraft().getTeamsList().get(g).setMoneyLeft(String.valueOf(teamMoneyLeft + playerSalary));

                    dataManager.getDraft().getPlayersList().add(ftScreen.getTable().getSelectionModel().getSelectedItem());

                    //remove the player from the list
                    dataManager.getDraft().getTeamsList().get(g).getPlayersList().remove(ftScreen.getTable().getSelectionModel().getSelectedItem());

                    //remove the player from the draft list
                    dataManager.getDraft().getDraftList().remove(ftScreen.getTable().getSelectionModel().getSelectedItem());
                }
            }
        });

        getFtScreen().getAddTeamButton().setOnAction(e -> {
            //fileController.handleAddTeamRequest(this);
            newFantasyTeamDialog.showAndWait();
            ftScreen.getComboBox().getSelectionModel().select(0);

        });

        getDsScreen().getDraftPlayerButton().setOnAction(e -> {

            //if ALL teams are NOT full draft a reg player
            if (!teamsAreFull(getDataManager().getDraft().getTeamsList())) {
                //find the first non fullteam in the list
                for (int i = 0; i < dataManager.getDraft().getTeamsList().size(); i++) {
                    if (!dataManager.getDraft().getTeamsList().get(i).isFull()) {
                        //find the next suitable player
                        Player p = findSuitablePlayer(getDataManager().getDraft().getTeamsList().get(i));
                        draftSuitablePlayer(p, getDataManager().getDraft().getTeamsList().get(i));
                    }
                }
            } //if all teams ARE FULL
            else {
                //find the first non full taxi team in the list
                for (int i = 0; i < dataManager.getDraft().getTeamsList().size(); i++) {
                    //if the taxi squad is not full
                    if (!dataManager.getDraft().getTeamsList().get(i).isTaxiFull()) {
                        Player p = dataManager.getDraft().getPlayersList().get(0);
                        p.setChosenPosition(p.getPositions());
                        taxiSuitablePlayer(p, getDataManager().getDraft().getTeamsList().get(i));
                    }
                }
            }
        });

        //pauses the draft
        getDsScreen().getPauseDraftButton().setOnAction(e->{
            draftPaused = true;
        });
        //starts the thread for a draft
        getDsScreen().getStartDraftButton().setOnAction(e -> {
            Task<Void> task = new Task<Void>() {
                @Override
                protected Void call() throws Exception {
                    draftPaused = false;
                    int i = 0;
                    int key = 0;
                    //while ALL teams are NOT full
                    while (!teamsAreFull(getDataManager().getDraft().getTeamsList())) {
                        System.out.println("Drafting:");
                        //fill up each team one at a time
                        while (!getDataManager().getDraft().getTeamsList().get(i).isFull()) {
                           if(draftPaused == true)
                               return null;
                            //find the next suitable player
                            Player p = findSuitablePlayer(getDataManager().getDraft().getTeamsList().get(i));

                            draftSuitablePlayer(p, getDataManager().getDraft().getTeamsList().get(i));
                            key++;
                            Thread.sleep(1000);
                            System.out.println("is team full? " + getDataManager().getDraft().getTeamsList().get(i).isFull());
                            System.out.println("all teams full? " + teamsAreFull(getDataManager().getDraft().getTeamsList()));
                           
                        }
                        i++;
                    }
                    //now that teams are full do the taxi draft
                    while (!taxiTeamsAreFull(getDataManager().getDraft().getTeamsList())) {
                        //find the first non full taxi team in the list
                        for (int t = 0; t < dataManager.getDraft().getTeamsList().size(); t++) {
                            //if the taxi squad is not full
                            if (!dataManager.getDraft().getTeamsList().get(t).isTaxiFull()) {
                                if(draftPaused == true)
                                    return null;
                                Player h = dataManager.getDraft().getPlayersList().get(0);
                                h.setChosenPosition(h.getPositions());
                                Thread.sleep(1000);
                                taxiSuitablePlayer(h, getDataManager().getDraft().getTeamsList().get(t));
                            }
                        }
                    }
                    return null;
                }
            };
            Thread thread = new Thread(task);
            thread.start();

        });

        //@todo if the update draft summary if team deleted
        getFtScreen().getRemoveTeamButton().setOnAction(e -> {
            //if the selection is not empty
            if (ftScreen.getComboBox().getSelectionModel().getSelectedItem() != null) {
                String s = ftScreen.getComboBox().getSelectionModel().getSelectedItem().toString();
                //if the list has only one item clear the list
                if (dataManager.getDraft().getTeamsList().size() == 1) {
                    FantasyTeam team = dataManager.getDraft().getTeamsList().get(0);
                    for (int i = 0; i < team.getPlayersList().size(); i++) {
                        dataManager.getDraft().getPlayersList().add(team.getPlayersList().get(i));
                        System.out.println(team.getPlayersList().get(i).getFirstName());
                    }
                    dataManager.getDraft().getTeamsList().clear();
                    ftScreen.getComboBox().setValue("");
                } //if the list is not empty
                else if (!dataManager.getDraft().getTeamsList().isEmpty()) {
                    //find the item
                    for (Iterator<FantasyTeam> it = dataManager.getDraft().getTeamsList().iterator(); it.hasNext();) {
                        FantasyTeam team = it.next();
                        if (team.getTeamName().equals(s)) {
                            //take all of the players and move them to the available players
                            for (int i = 0; i < team.getPlayersList().size(); i++) {
                                dataManager.getDraft().getPlayersList().add(team.getPlayersList().get(i));
                            }
                            it.remove();
                        }
                    }
                }
                //clear the draft summary items
                for (Iterator<Player> dsi = dataManager.getDraft().getDraftList().iterator(); dsi.hasNext();) {
                    if(dsi.next().getFantasyTeam().equals(ftScreen.getComboBox().getSelectionModel().getSelectedItem().toString()))
                    dataManager.getDraft().getDraftList().remove(dsi.next());
                }
            }
            ftScreen.getTable().getItems().clear();
        });

        getFtScreen().getEditTeamButton().setOnAction(e -> {
            EditTeamDialog editTeamDialog = new EditTeamDialog(primaryStage);

            for (int i = 0; i < dataManager.getDraft().getTeamsList().size(); i++) {
                //System.out.println(ftScreen.getComboBox().getValue().toString() + "l");
                //System.out.println(dataManager.getDraft().getTeamsList().get(0).getTeamName() + "l");
                //find the team with the name from the combo box
                if (dataManager.getDraft().getTeamsList().get(i).getTeamName().equals(
                        ftScreen.getComboBox().getValue().toString())) {
                    System.out.println(ftScreen.getComboBox().getSelectionModel().toString());
                    editTeamDialog.show(dataManager.getDraft().getTeamsList().get(i));
                }
            }
            ftScreen.refreshComboBox();

        });

        getFtScreen().getComboBox().valueProperty()
                .addListener(new ChangeListener<String>() {
                    @Override
                    public void changed(ObservableValue ov, String t, String t1) {
                        for (int i = 0; i < dataManager.getDraft().getTeamsList().size(); i++) {
                            if (ftScreen.getComboBox().getValue() != null) {
                                if (dataManager.getDraft().getTeamsList().get(i).getTeamName()
                                .equals(ftScreen.getComboBox().getValue().toString())) {

                                    ftScreen.getTable().setItems(dataManager.getDraft().getTeamsList().get(i).getPlayersList());
                                    ftScreen.getTaxiTable().setItems(dataManager.getDraft().getTeamsList().get(i).getTaxiList());
                                }
                            }
                        }

                    }
                });

        getmScreen().getComboBox().valueProperty()
                .addListener(new ChangeListener<String>() {
                    @Override
                    public void changed(ObservableValue ov, String t, String t1) {
                        ObservableList<Player> teamPlayersList = FXCollections.observableArrayList();
                        for (int i = 0; i < dataManager.getDraft().getPlayersList().size(); i++) {
                            if (dataManager.getDraft().getPlayersList().get(i).getProTeam()
                            .equals(mScreen.getComboBox().getValue().toString())) {
                                //add the player to the new list
                                teamPlayersList.add(dataManager.getDraft().getPlayersList().get(i));
                            }
                        }
                        Collections.sort(teamPlayersList, new Comparator<Player>() {
                            public int compare(Player p1, Player p2) {
                                return p1.getLastName().compareTo(p2.getLastName());
                            }
                        });
                        mScreen.getPlayersTable()
                        .setItems(teamPlayersList);

                    }
                });
    }

    /**
     * @return the ftScreen
     */
    public FantasyTeamsScreen getFtScreen() {
        return ftScreen;
    }

    /**
     * @return the apScreen
     */
    public AvailablePlayersScreen getApScreen() {
        return apScreen;
    }

    /**
     * @return the fsScreen
     */
    public FantasyStandingsScreen getFsScreen() {
        return fsScreen;
    }

    /**
     * @return the dsScreen
     */
    public DraftSummaryScreen getDsScreen() {
        return dsScreen;
    }

    /**
     * @return the mScreen
     */
    public MlbTeamsScreen getmScreen() {
        return mScreen;
    }

    public BorderPane getWbdPane() {
        return wbdPane;
    }

    public boolean taxiTeamsAreFull(ObservableList<FantasyTeam> list) {
        boolean full = false;

        for (int i = 0; i < list.size(); i++) {
            if (list.get(i).isTaxiFull()) {
                full = true;
            } else {
                full = false;
            }
        }
        return full;
    }

    public boolean teamsAreFull(ObservableList<FantasyTeam> list) {
        boolean full = false;

        for (int i = 0; i < list.size(); i++) {
            if (list.get(i).isFull()) {
                full = true;
            } else {
                full = false;
            }
        }
        return full;
    }

    public void draftSuitablePlayer(Player p, FantasyTeam team) {

        //set the fantasyteam field of the player
        p.setFantasyTeam(team.getTeamName());

        //set the contract field to S2
        p.setContract("S2");

        //set the player salary to 1
        p.setSalary("1");

        //add the player to the fantasy team
        team.addPlayer(p);

        //add player to the draft screen
        getDataManager().getDraft().getDraftList().add(p);

        //update the money left of the team
        //team.setMoneyLeft(String.valueOf(Integer.valueOf(team.getMoneyLeft()) - 1));
        //remove from the available players list
        getDataManager().getDraft().getPlayersList().remove(p);
        System.out.println(p.getFirstName() + " " + p.getLastName());

    }

    public Player findSuitablePlayer(FantasyTeam team) {
        for (int i = 0; i < getDataManager().getDraft().getPlayersList().size(); i++) {
            Player p = getDataManager().getDraft().getPlayersList().get(i);
            String convert = p.getPositions();

            //all the positions the player can be in
            ArrayList<String> allPositions = new ArrayList<String>(Arrays.asList(convert.split("_")));
            System.out.println("===" + p.getFirstName() + p.getLastName());
            //loop through the positions available
            for (int k = 0; k < allPositions.size(); k++) {
                System.out.println(allPositions.get(k) + " Valid Choice? " + team.isValidChoice(allPositions.get(k)));
                //if the player can be added to the team without conflict
                if (team.checkForValidPlayerChoice(allPositions.get(k))) {
                    //set the player chosen position
                    p.setChosenPosition(allPositions.get(k));
                    System.out.println("player: " + allPositions.get(k));
                    return p;
                }
            }

        }
        System.out.println("NO PLAYER FOUND");
        return null;

    }

    public void taxiSuitablePlayer(Player p, FantasyTeam team) {

        //set the fantasyteam field of the player
        p.setFantasyTeam(team.getTeamName());

        //set the contract field to S2
        p.setContract("X");

        //set the player salary to 1
        p.setSalary("1");

        //add the player to the fantasy team
        team.addTaxiPlayer(p);

        //add player to the draft screen
        getDataManager().getDraft().getDraftList().add(p);

        //update the money left of the team
        //team.setMoneyLeft(String.valueOf(Integer.valueOf(team.getMoneyLeft()) - 1));
        //remove from the available players list
        getDataManager().getDraft().getPlayersList().remove(p);

    }
}
