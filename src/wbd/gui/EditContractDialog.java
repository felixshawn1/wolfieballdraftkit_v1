package wbd.gui;

import javafx.geometry.Insets;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.Label;
import javafx.scene.layout.VBox;
import javafx.stage.Modality;
import javafx.stage.Stage;
import wbd.data.Player;

/**
 *
 * @author Shawn Felix <shawnfelixcodes.wordpress.com>
 */
public class EditContractDialog extends Stage {
    VBox pane;
    Label headingLabel;
    ComboBox box;
    Scene messageScene;
    Button completeButton;
    
    public EditContractDialog(Stage owner, Player player, WBD_GUI gui){
        this.setTitle("Edit Contract");
        
        // MAKE IT MODAL
        initModality(Modality.WINDOW_MODAL);
        initOwner(owner);
        
        pane = new VBox();
        headingLabel = new Label("Edit " + player.getFirstName() + " " + player.getLastName() + "'s Contract");
        box = new ComboBox();
        completeButton = new Button("Complete");
        box.getItems().add("S2");
        box.getItems().add("S1");
        box.getItems().add("X");
        box.setValue(player.getContract());
        
        pane.getChildren().add(headingLabel);
        pane.getChildren().add(box);
        pane.getChildren().add(completeButton);
        
        completeButton.setOnAction(e->{
            Boolean changed = true;
            
            
            if(player.getContract().equals("S2"))
                changed = false;
            
            player.setContract(box.getSelectionModel().selectedItemProperty().get().toString());
            //if the player's contract is NOT S2
            if(!gui.getFtScreen().getTable().getSelectionModel().getSelectedItem().getContract().equals("S2")){
                //remove it from the list         
                gui.getDsScreen().getDraftTable().getItems().remove(gui.getFtScreen().getTable().getSelectionModel().getSelectedItem());
            }
            else{
                gui.getDsScreen().getDraftTable().getItems().add(player);
            }
            //refreshes the table to update the value
            gui.getFtScreen().getTable().getColumns().get(6).setVisible(false);
            gui.getFtScreen().getTable().getColumns().get(6).setVisible(true);
            this.close();
        });
        pane.setPadding(new Insets(10, 20, 20, 20));
        pane.setSpacing(10);
        
        messageScene = new Scene(pane);
        
        this.setScene(messageScene);
        
    }
    
    
    public void show(String message) {
        this.showAndWait();
    }
}
