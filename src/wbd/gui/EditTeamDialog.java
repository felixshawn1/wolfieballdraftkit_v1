package wbd.gui;

import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.VBox;
import javafx.stage.Modality;
import javafx.stage.Stage;
import wbd.data.FantasyTeam;

/**
 *
 * @author Shawn Felix <shawnfelixcodes.wordpress.com>
 */
public class EditTeamDialog extends Stage {
    VBox messagePane;
    Scene messageScene;
    Label headingLabel;
    
    GridPane pane;
    Label teamNameLabel;
    TextField teamNameText;
    Label teamOwnerLabel;
    TextField teamOwnerText;
    
    Button completeButton;
    Button cancelButton;
    
    /**
     * Initializes this dialog so that it can be used repeatedly
     * for all kinds of messages.
     * 
     * @param owner The owner stage of this modal dialog.
     * 
     * @param cancelButtonText Text to appear on the cancel button.
     */
    public EditTeamDialog(Stage owner) {
        this.setTitle("Edit Fantasy Team");
        
        // MAKE IT MODAL
        initModality(Modality.WINDOW_MODAL);
        initOwner(owner);
        
        messagePane = new VBox();
        // MAKE IT LOOK NICE
        messagePane.setPadding(new Insets(10, 20, 20, 20));
        messagePane.setSpacing(10);

        // AND PUT IT IN THE WINDOW
        messageScene = new Scene(messagePane);
        this.setScene(messageScene);
    }
 
    /**
     * This method loads a custom message into the label and
     * then pops open the dialog.
     * 
     * @param message Message to appear inside the dialog.
     */
    public void show(FantasyTeam teamToEdit) {
        pane = new GridPane();
        headingLabel = new Label("Edit Team");
        teamNameLabel = new Label("Team Name:");
        teamNameText = new TextField(teamToEdit.getTeamName());
        teamOwnerLabel = new Label("Team Owner:");
        teamOwnerText = new TextField(teamToEdit.getOwnerName());
        
        pane.add(teamNameLabel, 0, 0);
        pane.add(teamNameText, 1, 0);
        pane.add(teamOwnerLabel, 0, 1);
        pane.add(teamOwnerText, 1, 1);
        
        completeButton = new Button("Complete");
        completeButton.setOnAction(e->{
            teamToEdit.setTeamName(teamNameText.getText());
            teamToEdit.setOwnerName(teamOwnerText.getText());
            
            EditTeamDialog.this.close();
        });
        // CLOSE BUTTON
        cancelButton = new Button("Cancel");
        cancelButton.setOnAction(e->{ EditTeamDialog.this.close(); });
        
        pane.add(completeButton, 0,2);
        pane.add(cancelButton, 1,2);
        // WE'LL PUT EVERYTHING HERE
        
        messagePane.setAlignment(Pos.CENTER);
        messagePane.getChildren().add(headingLabel);
        messagePane.getChildren().add(pane);
        
        this.showAndWait();
    }
}
