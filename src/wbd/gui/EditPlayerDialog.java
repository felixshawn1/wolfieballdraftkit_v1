package wbd.gui;

import java.util.ArrayList;
import java.util.Arrays;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.geometry.Insets;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.FlowPane;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.VBox;
import javafx.stage.Modality;
import javafx.stage.Stage;
import wbd.data.FantasyTeam;
import wbd.data.Hitter;
import wbd.data.Player;

/**
 *
 * @author Shawn Felix <shawnfelixcodes.wordpress.com>
 */
public class EditPlayerDialog extends Stage {

    WBD_GUI gui;

    String firstName;
    String lastName;
    String country;
    String positions;

    VBox messagePane;
    Scene messageScene;
    BorderPane picPane;
    VBox flagBox;
    GridPane controlsPane;

    Label headingLabel;
    Label playerLabel;
    Label positionsLabel;

    Image playerImage;
    Image flagImage;

    ImageView playerImageView;
    ImageView flagImageView;

    Label ftLabel;
    ComboBox ftBox;
    Label posLabel;
    ComboBox posBox;
    Label contractLabel;
    ComboBox contractBox;
    Label salaryLabel;
    TextField salaryText;

    Button completeButton;
    Button cancelButton;

    public EditPlayerDialog(Stage owner, WBD_GUI gui) {
        this.setTitle("Edit Player");
        this.gui = gui;

        initModality(Modality.WINDOW_MODAL);
        initOwner(owner);

        messagePane = new VBox();
        headingLabel = new Label();
        picPane = new BorderPane();
        headingLabel.setText("Player Details");
        messagePane.getChildren().add(headingLabel);

        // MAKE IT LOOK NICE
        // AND PUT IT IN THE WINDOW
        messageScene = new Scene(messagePane);
        this.setScene(messageScene);
    }

    /**
     * This method loads a custom message into the label and then pops open the
     * dialog.
     *
     * @param p
     */
    public void show(Player p) {
        firstName = p.getFirstName();
        lastName = p.getLastName();
        country = p.getBirthNation();
        positions = p.getPositions();
        String playerImageLocation = "file:./images/players/" + lastName + firstName + ".jpg";
        String flagImageLocation = "file:./images/flags/" + country + ".png";

        playerImage = new Image(playerImageLocation);
        flagImage = new Image(flagImageLocation);

        controlsPane = new GridPane();
        flagBox = new VBox();

        //IF THE IMAGE DNE, USE THE GENERIC PHOTOMISSING
        if (playerImage.getWidth() == 0) {
            playerImage = new Image("file:./images/players/AAA_PhotoMissing.jpg");
        }

        playerImageView = new ImageView();
        flagImageView = new ImageView();
        playerImageView.setImage(playerImage);
        flagImageView.setImage(flagImage);

        playerLabel = new Label(firstName + " " + lastName);
        posLabel = new Label(positions);
        flagBox.getChildren().add(flagImageView);
        flagBox.getChildren().add(playerLabel);
        flagBox.getChildren().add(posLabel);

        ObservableList<String> options
                = FXCollections.observableArrayList(
                        "S2",
                        "S1",
                        "X"
                );

        ftLabel = new Label("Fantasy Team:");
        posLabel = new Label("Position:");
        contractLabel = new Label("Contract:");
        salaryLabel = new Label("Salary ($):");
        ftBox = new ComboBox();
        posBox = new ComboBox();
        contractBox = new ComboBox(options);
        salaryText = new TextField();

        ftBox.setMinWidth(200);
        posBox.setMinWidth(200);
        contractBox.setMinWidth(200);
        salaryText.setMinWidth(200);

        controlsPane.add(ftLabel, 0, 0);
        controlsPane.add(ftBox, 1, 0);
        controlsPane.add(posLabel, 0, 1);
        controlsPane.add(posBox, 1, 1);
        controlsPane.add(contractLabel, 0, 2);
        controlsPane.add(contractBox, 1, 2);
        controlsPane.add(salaryLabel, 0, 3);
        controlsPane.add(salaryText, 1, 3);

        ObservableList<FantasyTeam> list = gui.getDataManager().getDraft().getTeamsList();
        ftBox.getItems().clear();
        for (int i = 0; i < list.size(); i++) {
            list.get(i).getTeamName();

            ftBox.getItems().add(list.get(i).getTeamName());
        }

        String convert = gui.getApScreen().getPlayerTableView().getSelectionModel().getSelectedItem().getPositions();
        ArrayList<String> allPositions = new ArrayList<String>(Arrays.asList(convert.split("_")));
        ObservableList<String> allPositionsO = FXCollections.observableArrayList(allPositions);
        posBox.setItems(allPositionsO);

        completeButton = new Button("Complete");
        cancelButton = new Button("Cancel");

        completeButton.setOnAction(e -> {
            //loop through all the teams
            FantasyTeam team = gui.getDataManager().getDraft().getTeamsList().get(0);
                for (int i = 0; i < gui.getDataManager().getDraft().getTeamsList().size(); i++) {
                    //find the team the user chose
                    if (gui.getDataManager().getDraft().getTeamsList().get(i).getTeamName()
                            .equals(ftBox.getValue().toString())) {
                        team = gui.getDataManager().getDraft().getTeamsList().get(i);
                    }
                    }
            //if the user left some blanks in the textfields
            if (posBox.getValue() == null || ftBox.getValue() == null || contractBox.getValue() == null || salaryText.getText().equals("") 
                    || (contractBox.getSelectionModel().getSelectedItem().toString().equals("X") 
                    && !team.isFull())) {
                //do nothing
            } //otherwise, make the player
            else {
                //set the values of the player taken from the user input
                gui.getApScreen().getPlayerTableView().getSelectionModel().getSelectedItem()
                        .setSalary(salaryText.getText());
                gui.getApScreen().getPlayerTableView().getSelectionModel().getSelectedItem()
                        .setContract(contractBox.getValue().toString());
                gui.getApScreen().getPlayerTableView().getSelectionModel().getSelectedItem()
                        .setChosenPosition(posBox.getValue().toString());
                //loop through all the teams
                for (int i = 0; i < gui.getDataManager().getDraft().getTeamsList().size(); i++) {
                    //find the team the user chose
                    if (gui.getDataManager().getDraft().getTeamsList().get(i).getTeamName()
                            .equals(ftBox.getValue().toString())) {
                        //if there are less than 23 people
                        if (gui.getDataManager().getDraft().getTeamsList().get(i).getPlayersList().size() < 23) {
                            //if the player can be added to the team without conflict
                            String boxChoice = posBox.getSelectionModel().getSelectedItem().toString();
                            if (gui.getDataManager().getDraft().getTeamsList().get(i).checkForValidPlayerChoice(boxChoice)) {

                                //set the fantasyTeam field of the player
                                gui.getApScreen().getPlayerTableView().getSelectionModel().getSelectedItem()
                                        .setFantasyTeam(gui.getDataManager().getDraft().getTeamsList().get(i).getTeamName());

                                //add the player to the fantasy team
                                gui.getDataManager().getDraft().getTeamsList().get(i)
                                        .addPlayer(gui.getApScreen().getPlayerTableView().getSelectionModel().getSelectedItem());

                                //add the player to the draft screen if contract is S2
                                if (contractBox.getSelectionModel().getSelectedItem().toString().equals("S2")) {
                                    gui.getDataManager().getDraft().getDraftList()
                                            .add(gui.getApScreen().getPlayerTableView().getSelectionModel().getSelectedItem());
                                }

                                //update the money left of the team
                                gui.getDataManager().getDraft().getTeamsList().get(i)
                                        .setMoneyLeft(String.valueOf(Integer.valueOf(gui.getDataManager().getDraft().getTeamsList().get(i).getMoneyLeft()) - Integer.valueOf(salaryText.getText())));

                                //remove from the available players list
                                gui.getDataManager().getDraft().getPlayersList()
                                        .remove(gui.getApScreen().getPlayerTableView().getSelectionModel().getSelectedItem());
                            } else {
                                System.out.println("Invalid Choice");
                            }
                        } //if there are 23 people or more, move player to the taxi squad
                        else {
                            System.out.println("YOU GOOD FAM");
                            for (int n = 0; n < gui.getDataManager().getDraft().getTeamsList().size(); n++) {
                                //find the team the user chose
                                if (gui.getDataManager().getDraft().getTeamsList().get(n).getTeamName()
                                        .equals(ftBox.getValue().toString())) {
                                    //set the fantasyTeam field of the player
                                    gui.getApScreen().getPlayerTableView().getSelectionModel().getSelectedItem()
                                            .setFantasyTeam(gui.getDataManager().getDraft().getTeamsList().get(n).getTeamName());
                                    //set the salary to 1
                                    gui.getApScreen().getPlayerTableView().getSelectionModel().getSelectedItem()
                                            .setSalary("1");
                                    //add the player to the fantasy taxi team
                                    gui.getDataManager().getDraft().getTeamsList().get(n)
                                            .addTaxiPlayer(gui.getApScreen().getPlayerTableView().getSelectionModel().getSelectedItem());
                                    //remove the player from available list
                                    gui.getDataManager().getDraft().getPlayersList()
                                            .remove(gui.getApScreen().getPlayerTableView().getSelectionModel().getSelectedItem());
                                }
                            }
                        }
                    }
                }
                EditPlayerDialog.this.close();
            }
        });

        cancelButton.setOnAction(e -> {
            EditPlayerDialog.this.close();
        });

        controlsPane.add(completeButton, 0, 4);
        controlsPane.add(cancelButton, 1, 4);
        //put all of it into a borderPane
        picPane.setLeft(playerImageView);
        picPane.setCenter(flagBox);
        picPane.setBottom(controlsPane);
        //put it into the main pane
        messagePane.getChildren().add(picPane);
        messagePane.setPadding(new Insets(10, 20, 20, 20));
        messagePane.setSpacing(10);

        this.showAndWait();

    }

    public void reset() {
        messagePane.getChildren().remove(picPane);
    }
}
