package wbd.gui.screens;

import javafx.scene.control.Label;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.layout.Pane;
import javafx.scene.layout.VBox;
import wbd.WBD_PropertyType;
import wbd.data.DraftDataManager;
import wbd.data.FantasyTeam;
import wbd.data.Player;
import wbd.gui.WBD_GUI;

/**
 *
 * @author Shawn Felix <shawnfelixcodes.wordpress.com>
 */
public class FantasyStandingsScreen extends WBDScreens{
    Label headingLabel;
    TableView<FantasyTeam> teamsTable;
    
    TableColumn teamName;
    TableColumn playersNeeded;
    TableColumn moneyLeft;
    TableColumn perPlayer;
    TableColumn runs;
    TableColumn homeruns;
    TableColumn rbis;
    TableColumn stolenBases;
    TableColumn battingAverage;
    TableColumn wins;
    TableColumn saves; //sv
    TableColumn strikeouts; //k
    TableColumn earnedRunAverage;
    TableColumn whip;
    TableColumn totalPoints;
    
    public FantasyStandingsScreen(DraftDataManager dataManager, Pane containerPane, WBD_GUI gui){
        initFantasyStandingsScreen(containerPane, gui);
    }
    
    public void initFantasyStandingsScreen(Pane containerPane, WBD_GUI gui){
        headingLabel = initChildLabel(mainPane, WBD_PropertyType.FANTASY_STANDINGS_LABEL, CLASS_HEADING_LABEL);
        containerPane.getStyleClass().add(CLASS_BORDERED_PANE);
        containerPane.getChildren().add(mainPane);
        
        initFantasyStandingsTable(gui);
    }
    
    public void initFantasyStandingsTable(WBD_GUI gui){
        //mainPane = new VBox();
        teamsTable = new TableView();
        
        
        teamName = new TableColumn("Team Name");
        playersNeeded = new TableColumn("Players Needed");
        moneyLeft = new TableColumn("$ Left");
        perPlayer = new TableColumn("$ PP");
        runs = new TableColumn("R");
        homeruns = new TableColumn("HR");
        rbis = new TableColumn("RBI");
        stolenBases = new TableColumn("SB");
        battingAverage = new TableColumn("BA");
        wins = new TableColumn("W");
        saves = new TableColumn("SV"); //sv
        strikeouts = new TableColumn("K"); //k
        earnedRunAverage = new TableColumn("ERA");
        whip = new TableColumn("WHIP");
        totalPoints = new TableColumn("Total Points");
        
        teamsTable.getColumns().add(teamName);
        teamsTable.getColumns().add(playersNeeded);
        teamsTable.getColumns().add(moneyLeft);
        teamsTable.getColumns().add(perPlayer);
        teamsTable.getColumns().add(runs);
        teamsTable.getColumns().add(homeruns);
        teamsTable.getColumns().add(rbis);
        teamsTable.getColumns().add(stolenBases);
        teamsTable.getColumns().add(battingAverage);
        teamsTable.getColumns().add(wins);
        teamsTable.getColumns().add(saves);
        teamsTable.getColumns().add(strikeouts);
        teamsTable.getColumns().add(earnedRunAverage);
        teamsTable.getColumns().add(whip);
        teamsTable.getColumns().add(totalPoints);
        
        teamName.setCellValueFactory(new PropertyValueFactory<>("teamName"));
        playersNeeded.setCellValueFactory(new PropertyValueFactory<>("playersNeeded"));
        moneyLeft.setCellValueFactory(new PropertyValueFactory<>("moneyLeft"));
        perPlayer.setCellValueFactory(new PropertyValueFactory<>("moneyPP"));
        runs.setCellValueFactory(new PropertyValueFactory<>("runs"));
        homeruns.setCellValueFactory(new PropertyValueFactory<>("homeRuns"));
        rbis.setCellValueFactory(new PropertyValueFactory<>("rbis"));
        stolenBases.setCellValueFactory(new PropertyValueFactory<>("stolenBases"));
        battingAverage.setCellValueFactory(new PropertyValueFactory<>("battingAverage"));
        wins.setCellValueFactory(new PropertyValueFactory<>("wins"));
        saves.setCellValueFactory(new PropertyValueFactory<>("saves"));
        strikeouts.setCellValueFactory(new PropertyValueFactory<>("strikeouts"));
        earnedRunAverage.setCellValueFactory(new PropertyValueFactory<>("era"));
        whip.setCellValueFactory(new PropertyValueFactory<>("whip"));
        totalPoints.setCellValueFactory(new PropertyValueFactory<>("totalPoints"));
        
        teamsTable.setItems(gui.getDataManager().getDraft().getTeamsList());
        
        mainPane.getChildren().add(teamsTable);
    }
    
    public TableView<FantasyTeam> getTeamsTable(){
        return teamsTable;
    }
}
