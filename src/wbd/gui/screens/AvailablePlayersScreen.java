/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package wbd.gui.screens;

import javafx.collections.ObservableList;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.RadioButton;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.control.ToggleGroup;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.layout.FlowPane;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.Pane;
import javafx.scene.layout.VBox;
import wbd.WBD_PropertyType;
import wbd.data.DraftDataManager;
import wbd.data.Player;
import wbd.gui.WBD_GUI;

/**
 *
 * @author Shawn
 */
public class AvailablePlayersScreen extends WBDScreens {
 
    FlowPane topPane;
    Button addPlayerButton;
    Button removePlayerButton;
    Label searchLabel;
    TextField searchBar;
    
    FlowPane playerSortPane;
    //TOGGLE-ABLE RADIO BUTTON GROUPS
    ToggleGroup apGroup;
    RadioButton apAll;
    RadioButton apC;
    RadioButton ap1b;
    RadioButton apCl;
    RadioButton ap3b;
    RadioButton ap2b;
    RadioButton apMi;
    RadioButton apSs;
    RadioButton apOf;
    RadioButton apU;
    RadioButton apP;
    
    TableView<Player> playersTable;
    TableColumn firstNameColumn;
    TableColumn lastNameColumn;
    TableColumn proTeamColumn;
    TableColumn positionsColumn;
    TableColumn yearOfBirthColumn;
    TableColumn rwColumn;
    TableColumn hrsvColumn;
    TableColumn rbikColumn;
    TableColumn sberaColumn;
    TableColumn bawhipColumn;
    TableColumn estimatedValueColumn;
    TableColumn notesColumn;
    
    

    public AvailablePlayersScreen(DraftDataManager dataManager, Pane containerPane, ObservableList<Player> list) {
        initAvailablePlayersScreen(dataManager, containerPane, list);
    }
    public void initAvailablePlayersScreen(DraftDataManager dataManager, Pane containerPane, ObservableList<Player> list){
        //CONTAINS THE HEADING LABEL, PLUS MINUS, AND SEARCH BAR
       
        headingLabel = initChildLabel(mainPane, WBD_PropertyType.AVAILABLE_PLAYERS_LABEL, CLASS_HEADING_LABEL);
        topPane = new FlowPane();
        
        addPlayerButton = initChildButton(topPane, WBD_PropertyType.ADD_ICON, WBD_PropertyType.ADD_PLAYER_TOOLTIP, false);
        removePlayerButton = initChildButton(topPane, WBD_PropertyType.MINUS_ICON, WBD_PropertyType.REMOVE_PLAYER_TOOLTIP, false);
        searchLabel = initChildLabel(topPane, WBD_PropertyType.SEARCH_LABEL, CLASS_HEADING_LABEL);
        searchBar = new TextField();
        topPane.getChildren().add(searchBar);
        
        mainPane.getChildren().add(topPane);
        initPlayerSortPane();
        
        
        mainPane.getChildren().add(playerSortPane);
        
        playersTable = new TableView();
        mainPane.getChildren().add(playersTable);
        
        firstNameColumn = new TableColumn("First Name");
        lastNameColumn = new TableColumn("Last Name");
        proTeamColumn = new TableColumn("Pro Team");
        positionsColumn = new TableColumn("Positions");
        yearOfBirthColumn = new TableColumn("Year of Birth");
        rwColumn = new TableColumn("R/W");
        hrsvColumn = new TableColumn("HR/SV");
        rbikColumn = new TableColumn("RBI/K");
        sberaColumn = new TableColumn("SB/ERA");
        bawhipColumn = new TableColumn("BA/WHIP");
        estimatedValueColumn = new TableColumn("Estimated Value");
        notesColumn = new TableColumn("Notes");
        
        
        //PLACE THE COLUMNS IN THE TABLEVIEW
        playersTable.getColumns().add(firstNameColumn);
        playersTable.getColumns().add(lastNameColumn);
        playersTable.getColumns().add(proTeamColumn);
        playersTable.getColumns().add(positionsColumn);
        playersTable.getColumns().add(yearOfBirthColumn);
        playersTable.getColumns().add(rwColumn);
        playersTable.getColumns().add(hrsvColumn);
        playersTable.getColumns().add(rbikColumn);
        playersTable.getColumns().add(sberaColumn);
        playersTable.getColumns().add(bawhipColumn);
        playersTable.getColumns().add(estimatedValueColumn);
        playersTable.getColumns().add(notesColumn);
        
        firstNameColumn.setCellValueFactory(new PropertyValueFactory<>("firstName"));
        lastNameColumn.setCellValueFactory(new PropertyValueFactory<>("lastName"));
        proTeamColumn.setCellValueFactory(new PropertyValueFactory<>("proTeam"));
        positionsColumn.setCellValueFactory(new PropertyValueFactory<>("positions"));
        yearOfBirthColumn.setCellValueFactory(new PropertyValueFactory<>("birthYear"));
        rwColumn.setCellValueFactory(new PropertyValueFactory<>("rw"));
        hrsvColumn.setCellValueFactory(new PropertyValueFactory<>("hrsv"));
        rbikColumn.setCellValueFactory(new PropertyValueFactory<>("rbik")); 
        sberaColumn.setCellValueFactory(new PropertyValueFactory<>("sbera"));
        bawhipColumn.setCellValueFactory(new PropertyValueFactory<>("bawhip"));
        //estimatedValueColumn.setCellValueFactory(new PropertyValueFactory<>(""));
        notesColumn.setCellValueFactory(new PropertyValueFactory<>("notes"));
        
        playersTable.setItems(list);

        containerPane.getStyleClass().add(CLASS_BORDERED_PANE);
        containerPane.getChildren().add(mainPane);
        
    }
    
    private void initPlayerSortPane(){
        playerSortPane = new FlowPane();
        playerSortPane.getStyleClass().add(CLASS_BORDERED_PANE);
        
        //INIT THE GROUP OF RADIO BUTTONS
        apGroup = new ToggleGroup();
        
        initRadioButton(apAll, playerSortPane, apGroup, "All     ", true);
        initRadioButton(apC, playerSortPane, apGroup,"C     ",false);
        initRadioButton(ap1b, playerSortPane, apGroup,"1B     ",false);
        initRadioButton(apCl, playerSortPane, apGroup,"Cl     ",false);
        initRadioButton(ap3b, playerSortPane, apGroup,"3B     ",false);
        initRadioButton(ap2b, playerSortPane, apGroup,"2B     ",false);
        initRadioButton(apMi, playerSortPane, apGroup,"MI     ",false);
        initRadioButton(apSs, playerSortPane, apGroup,"SS     ",false);
        initRadioButton(apOf, playerSortPane, apGroup,"OF     ",false);
        initRadioButton(apU, playerSortPane, apGroup, "U     ",false);
        initRadioButton(apP, playerSortPane, apGroup,"P     ",false);
        
    }
    
    public TableView<Player> getPlayerTableView(){
        return playersTable;
    }
    
    public Button getAddPlayerButton(){
        return addPlayerButton;
    }
    
    public Button getRemovePlayerButton(){
        return removePlayerButton;
    }
}
