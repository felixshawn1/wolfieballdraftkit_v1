package wbd.gui.screens;

import java.util.List;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.scene.control.ComboBox;
import javafx.scene.control.Label;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.layout.FlowPane;
import javafx.scene.layout.Pane;
import wbd.WBD_PropertyType;
import wbd.data.DraftDataManager;
import wbd.data.FantasyTeam;
import wbd.data.Player;
import wbd.gui.WBD_GUI;

/**
 *
 * @author Shawn Felix <shawnfelixcodes.wordpress.com>
 */
public class MlbTeamsScreen extends WBDScreens {
    Label selectTeamLabel;
    ComboBox selectTeamComboBox;
    TableView<Player> playersTable;
    ObservableList<String> teamsList;
    
    FlowPane selectTeamPane;
    
    TableColumn firstName;
    TableColumn lastName;
    TableColumn positions;
    
    DraftDataManager dataManager;
    
    public MlbTeamsScreen(DraftDataManager dataManager, Pane containerPane, WBD_GUI gui){
        initMlbTeamsScreen(containerPane, gui);
        initTeamsList();
        
    }
    
    public void initMlbTeamsScreen(Pane containerPane, WBD_GUI gui){
        teamsList = FXCollections.observableArrayList();
        headingLabel = initChildLabel(mainPane, WBD_PropertyType.MLB_TEAMS_LABEL, CLASS_HEADING_LABEL);
        containerPane.getStyleClass().add(CLASS_BORDERED_PANE);
        containerPane.getChildren().add(mainPane);
        
        selectTeamPane = new FlowPane();
        
        selectTeamLabel = new Label("Select Pro Team:");
        selectTeamComboBox = new ComboBox();
        dataManager = gui.getDataManager();
        
        setComboBoxItems(teamsList);
        
        selectTeamPane.getChildren().add(selectTeamLabel);
        selectTeamPane.getChildren().add(selectTeamComboBox);
        

        initPlayersTable();
        
        //put everything in the window
        mainPane.getChildren().add(selectTeamPane);
        mainPane.getChildren().add(playersTable);
    }
    
    public void initPlayersTable(){
        playersTable = new TableView();
        
        playersTable.setMaxWidth(450);
        playersTable.setMinHeight(800);
        
        firstName = new TableColumn("First");
        lastName = new TableColumn("Last");
        positions = new TableColumn("Positions");
        
        firstName.setMinWidth(150);
        positions.setMinWidth(150);
        lastName.setMinWidth(150);
        
        playersTable.getColumns().add(firstName);
        playersTable.getColumns().add(lastName);
        playersTable.getColumns().add(positions);
        
        firstName.setCellValueFactory(new PropertyValueFactory<>("firstName"));
        lastName.setCellValueFactory(new PropertyValueFactory<>("lastName"));
        positions.setCellValueFactory(new PropertyValueFactory<>("positions"));
        
    }
    
    public void setComboBoxItems(ObservableList<String> list){
        selectTeamComboBox.setItems(list);
    }
    
    public ComboBox getComboBox(){
        return selectTeamComboBox;
    }
    public TableView getPlayersTable(){
        return playersTable;
    }
    private void initTeamsList(){
        teamsList.add("AZ");
        teamsList.add("ATL");
        teamsList.add("CHC");
        teamsList.add("CIN");
        teamsList.add("COL");
        teamsList.add("LAD");
        teamsList.add("MIA");
        teamsList.add("MIL");
        teamsList.add("NYM");
        teamsList.add("PHI");
        teamsList.add("PIT");
        teamsList.add("SD");
        teamsList.add("SF");
        teamsList.add("STL");
        teamsList.add("TOR");
        teamsList.add("WSH");
    }
}
