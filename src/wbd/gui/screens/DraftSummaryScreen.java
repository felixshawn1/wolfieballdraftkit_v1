package wbd.gui.screens;

import javafx.scene.control.Button;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.layout.FlowPane;
import javafx.scene.layout.Pane;
import javafx.scene.layout.VBox;
import wbd.WBD_PropertyType;
import wbd.data.DraftDataManager;
import wbd.data.Player;


/**
 *
 * @author Shawn Felix <shawnfelixcodes.wordpress.com>
 */
public class DraftSummaryScreen extends WBDScreens{
    Button draftPlayerButton;
    Button startDraftButton;
    Button pauseDraftButton;
    FlowPane buttonPane;
    
    TableView<Player> draftTable;
    
    TableColumn pickNumber;
    TableColumn first;
    TableColumn last;
    TableColumn team;
    TableColumn pos;
    TableColumn contract;
    TableColumn salary;
    
    public DraftSummaryScreen(DraftDataManager dataManager, Pane containerPane){
        initDraftSummaryScreen(containerPane);
    }
    public TableView<Player> getDraftTable(){
        return draftTable;
    }
    
    public void initDraftSummaryScreen(Pane containerPane){
        headingLabel = initChildLabel(mainPane, WBD_PropertyType.DRAFT_SUMMARY_LABEL, CLASS_HEADING_LABEL);
        buttonPane = new FlowPane();
        
        
        draftPlayerButton = new Button("Draft Player");
        startDraftButton = new Button("Start");
        pauseDraftButton = new Button("Pause");
        
        buttonPane.getChildren().add(draftPlayerButton);
        buttonPane.getChildren().add(startDraftButton);
        buttonPane.getChildren().add(pauseDraftButton);
        
        initDraftTable();
        
        mainPane.getChildren().add(buttonPane);
        mainPane.getChildren().add(draftTable);
        containerPane.getStyleClass().add(CLASS_BORDERED_PANE);
        containerPane.getChildren().add(mainPane);
    }
    
    private void initDraftTable(){
        draftTable = new TableView();
        
        pickNumber = new TableColumn("Pick #");
        first = new TableColumn("First");
        last = new TableColumn("Last");
        team = new TableColumn("Team");
        pos = new TableColumn("Position");
        contract = new TableColumn("Contract");
        salary = new TableColumn("Salary($)");
        
        //draftTable.getColumns().add(pickNumber);
        draftTable.getColumns().add(first);
        draftTable.getColumns().add(last);
        draftTable.getColumns().add(pos);
        draftTable.getColumns().add(team);
        draftTable.getColumns().add(contract);
        draftTable.getColumns().add(salary);
        
        
        //pickNumber.setCellValueFactory(new PropertyValueFactory<>(""));
        first.setCellValueFactory(new PropertyValueFactory<>("firstName"));
        last.setCellValueFactory(new PropertyValueFactory<>("lastName"));
        team.setCellValueFactory(new PropertyValueFactory<>("fantasyTeam"));
        contract.setCellValueFactory(new PropertyValueFactory<>("contract"));
        salary.setCellValueFactory(new PropertyValueFactory<>("salary"));
        pos.setCellValueFactory(new PropertyValueFactory<>("chosenPosition"));
        
    }
     
    public Button getDraftPlayerButton(){
        return draftPlayerButton;
    }
     
    public Button getStartDraftButton(){
        return startDraftButton;
    }
     
    public Button getPauseDraftButton(){
        return pauseDraftButton;
    }
     
}    
