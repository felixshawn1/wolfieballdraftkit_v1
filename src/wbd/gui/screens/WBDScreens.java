package wbd.gui.screens;

import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.RadioButton;
import javafx.scene.control.ToggleGroup;
import javafx.scene.control.Tooltip;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.FlowPane;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.Pane;
import javafx.scene.layout.VBox;
import properties_manager.PropertiesManager;
import wbd.WBD_PropertyType;
import static wbd.WBD_StartupConstants.PATH_CSS;
import static wbd.WBD_StartupConstants.PATH_IMAGES;

/**
 *
 * @author Shawn Felix <shawnfelixcodes.wordpress.com>
 */
public class WBDScreens {
    VBox mainPane;
    Label headingLabel;
    
    static final String PRIMARY_STYLE_SHEET = PATH_CSS + "wbd_style.css";
    static final String CLASS_BORDERED_PANE = "bordered_pane";
    static final String CLASS_SUBJECT_PANE = "subject_pane";
    static final String CLASS_HEADING_LABEL = "heading_label";
    static final String CLASS_SUBHEADING_LABEL = "subheading_label";
    static final String CLASS_PROMPT_LABEL = "prompt_label";
    
    
    
    public WBDScreens(){
        mainPane = new VBox();
        mainPane.setMinHeight(1000);
        
    }
    
    public Label initChildLabel(Pane container, WBD_PropertyType labelProperty, String styleClass){
        Label label = initLabel(labelProperty, styleClass);
        container.getChildren().add(label);
        return label;
    }
    
    public Label initGridLabel(GridPane container, String styleClass, WBD_PropertyType labelProperty, int col, int row, int colSpan, int rowSpan){
        Label label = initLabel(labelProperty, styleClass);
        container.add(label, col, row, colSpan, rowSpan);
        return label;
    }
    
    public Label initLabel(WBD_PropertyType labelProperty, String styleClass){
        PropertiesManager props = PropertiesManager.getPropertiesManager();
        String labelText = props.getProperty(labelProperty);
        Label label = new Label(labelText);
        label.getStyleClass().add(styleClass);
        return label;
    }
    
    /**
     * This method initializes a button and adds it to the container in a toolbar
     * 
     * @param toolbar The toolbar that the button is to be added to
     * @param icon  The image that the button shall use
     * @param tooltip The tooltip that the button shall have
     * @param disabled Is the button 
     * @return 
     */
    public Button initChildButton(Pane toolbar, WBD_PropertyType icon, WBD_PropertyType tooltip, boolean disabled){
        PropertiesManager props = PropertiesManager.getPropertiesManager();
        String imagePath = "file:" + PATH_IMAGES + props.getProperty(icon.toString());
        Image buttonImage = new Image(imagePath);
        Button button = new Button();
        button.setDisable(disabled);
        button.setGraphic(new ImageView(buttonImage));
        Tooltip buttonTooltip = new Tooltip(props.getProperty(tooltip.toString()));
        button.setTooltip(buttonTooltip);
        toolbar.getChildren().add(button);
        return button;
    }
    
    public Button initButton(WBD_PropertyType icon, WBD_PropertyType tooltip, boolean disabled){
        PropertiesManager props = PropertiesManager.getPropertiesManager();
        String imagePath = "file:" + PATH_IMAGES + props.getProperty(icon.toString());
        Image buttonImage = new Image(imagePath);
        Button button = new Button();
        button.setDisable(disabled);
        button.setGraphic(new ImageView(buttonImage));
        Tooltip buttonTooltip = new Tooltip(props.getProperty(tooltip.toString()));
        button.setTooltip(buttonTooltip);
        return button;
    }
    
    public void initRadioButton(RadioButton button, FlowPane pane, ToggleGroup group, String text, boolean selected){
        button = new RadioButton();
        button.setToggleGroup(group);
        button.setSelected(selected);
        button.setText(text);
        pane.getChildren().add(button);
    }
}
