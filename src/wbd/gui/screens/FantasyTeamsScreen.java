/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package wbd.gui.screens;

import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.Label;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.layout.FlowPane;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.Pane;
import javafx.scene.layout.VBox;
import properties_manager.PropertiesManager;
import wbd.WBD_PropertyType;
import wbd.controller.DraftEditController;
import wbd.data.DraftDataManager;
import wbd.data.Player;
import wbd.gui.WBD_GUI;

/**
 *
 * @author Shawn
 */
public class FantasyTeamsScreen extends WBDScreens {
    WBD_GUI gui;
    DraftEditController draftController;
    
    FlowPane editPlayerButtonPane;
    FlowPane topPane;
    FlowPane underTopPane;
    
    Label draftNameLabel;
    public TextField draftNameBox;
    Button addTeamButton;
    Button removeTeamButton;
    Button editTeamButton;
    Label selectFantasyTeamLabel;
    ComboBox selectFantasyTeamComboBox;
    Button removePlayerButton;
    Button editPlayerContractButton;
    
    VBox lineupPane;
    Label startingLineupLabel;
    TableView<Player> lineupTable;
    
    VBox taxiPane;
    Label taxiSquadLabel;
    TableView<Player> taxiTable;
    Button removeTaxiButton;
    
    TableColumn chosenPositionColumn;
    TableColumn firstNameColumn;
    TableColumn lastNameColumn;
    TableColumn proTeamColumn;
    TableColumn positionsColumn;
    TableColumn rwColumn;
    TableColumn hrsvColumn;
    TableColumn rbikColumn;
    TableColumn sberaColumn;
    TableColumn bawhipColumn;
    TableColumn estimatedValueColumn;
    TableColumn contractColumn;
    TableColumn salaryColumn;
    
    public FantasyTeamsScreen(DraftDataManager dataManager, Pane containerPane, WBD_GUI gui){
        initFantasyTeamsScreen(containerPane);
        this.gui = gui;
    }
    
    public TextField getDraftNameTextField(){
        return draftNameBox;
    }
    
    public void initFantasyTeamsScreen(Pane containerPane){
        Label headingLabel = initChildLabel(mainPane, WBD_PropertyType.FANTASY_TEAMS_LABEL, CLASS_HEADING_LABEL);

        //Contains draft name plus minus, edit, and select fantasy team
        topPane = new FlowPane();
        underTopPane = new FlowPane();
        
        draftNameLabel = initChildLabel(topPane, WBD_PropertyType.DRAFT_NAME_LABEL, CLASS_SUBHEADING_LABEL);
        draftNameBox = new TextField(); //@todo
        draftNameBox.setPrefColumnCount(15);
        draftNameBox.setText("");
        draftNameBox.setEditable(true);
        
        
        topPane.getChildren().add(draftNameBox);
        
        addTeamButton = initChildButton(underTopPane, WBD_PropertyType.ADD_ICON, WBD_PropertyType.ADD_ITEM_TOOLTIP, false);
        removeTeamButton = initChildButton(underTopPane, WBD_PropertyType.MINUS_ICON, WBD_PropertyType.REMOVE_ITEM_TOOLTIP, false);
        editTeamButton = initChildButton(underTopPane, WBD_PropertyType.EDIT_ICON, WBD_PropertyType.EDIT_ITEM_TOOLTIP, false);
        
        selectFantasyTeamLabel = initChildLabel(underTopPane, WBD_PropertyType.SELECT_FANTASY_TEAM_LABEL, CLASS_SUBHEADING_LABEL);
        selectFantasyTeamComboBox = new ComboBox();//@todo
        underTopPane.getChildren().add(selectFantasyTeamComboBox);
        
        mainPane.getChildren().add(topPane);
        mainPane.getChildren().add(underTopPane);
        
        initStartingLineupPane();

        containerPane.getStyleClass().add(CLASS_BORDERED_PANE);
        containerPane.getChildren().add(mainPane);
        
        
    }
    
    public Button getAddTeamButton(){
        return addTeamButton;
    }
    
    public Button getRemoveTeamButton(){
        return removeTeamButton;
    }
    
    
    
    private void initStartingLineupPane(){
        lineupPane = new VBox();
        taxiPane = new VBox();
        startingLineupLabel = initChildLabel(lineupPane, WBD_PropertyType.STARTING_LINEUP_LABEL, CLASS_SUBHEADING_LABEL);
        taxiSquadLabel = initChildLabel(taxiPane, WBD_PropertyType.TAXI_SQUAD_LABEL, CLASS_SUBHEADING_LABEL);
        removeTaxiButton = new Button("Remove Selected Player");
        removePlayerButton = new Button("Remove Selected Player");
        editPlayerContractButton = new Button("Edit Contract");
        editPlayerButtonPane = new FlowPane();
        
        taxiPane.getChildren().add(removeTaxiButton);
        editPlayerButtonPane.getChildren().add(removePlayerButton);
        editPlayerButtonPane.getChildren().add(editPlayerContractButton);
        lineupPane.getChildren().add(editPlayerButtonPane);
        
        initLineupTable();
        
        initTaxiTable();
        
        lineupPane.getChildren().add(lineupTable);
        taxiPane.getChildren().add(taxiTable);
        
        //put it all in the container
        mainPane.getChildren().add(lineupPane);
        mainPane.getChildren().add(taxiPane);
        
        lineupPane.getStyleClass().add(CLASS_BORDERED_PANE);
        taxiPane.getStyleClass().add(CLASS_BORDERED_PANE);
        }
    
    
    private void initLineupTable(){
        lineupTable = new TableView();
        
        chosenPositionColumn = new TableColumn("Position");
        firstNameColumn = new TableColumn("First Name");
        lastNameColumn = new TableColumn("Last Name");
        proTeamColumn = new TableColumn("Pro Team");
        positionsColumn = new TableColumn("Positions");
        rwColumn = new TableColumn("R/W");
        hrsvColumn = new TableColumn("HR/SV");
        rbikColumn = new TableColumn("RBI/K");
        sberaColumn = new TableColumn("SB/ERA");
        bawhipColumn = new TableColumn("BA/WHIP");
        estimatedValueColumn = new TableColumn("Estimated Value");
        contractColumn = new TableColumn("Contract");
        salaryColumn = new TableColumn("Salary");
        
        lineupTable.getColumns().add(chosenPositionColumn);
        lineupTable.getColumns().add(firstNameColumn);
        lineupTable.getColumns().add(lastNameColumn);
        lineupTable.getColumns().add(proTeamColumn);
        lineupTable.getColumns().add(positionsColumn);
        lineupTable.getColumns().add(rwColumn);
        lineupTable.getColumns().add(hrsvColumn);
        lineupTable.getColumns().add(rbikColumn);
        lineupTable.getColumns().add(sberaColumn);
        lineupTable.getColumns().add(bawhipColumn);
        lineupTable.getColumns().add(estimatedValueColumn);
        lineupTable.getColumns().add(contractColumn);
        lineupTable.getColumns().add(salaryColumn);
        
        chosenPositionColumn.setCellValueFactory(new PropertyValueFactory<>("chosenPosition"));
        firstNameColumn.setCellValueFactory(new PropertyValueFactory<>("firstName"));
        lastNameColumn.setCellValueFactory(new PropertyValueFactory<>("lastName"));
        proTeamColumn.setCellValueFactory(new PropertyValueFactory<>("proTeam"));
        positionsColumn.setCellValueFactory(new PropertyValueFactory<>("positions"));
        rwColumn.setCellValueFactory(new PropertyValueFactory<>("rw"));
        hrsvColumn.setCellValueFactory(new PropertyValueFactory<>("hrsv"));
        rbikColumn.setCellValueFactory(new PropertyValueFactory<>("rbis"));
        rbikColumn.setCellValueFactory(new PropertyValueFactory<>("strikeouts")); 
        sberaColumn.setCellValueFactory(new PropertyValueFactory<>("sbera"));
        bawhipColumn.setCellValueFactory(new PropertyValueFactory<>("bawhip"));
        //estimatedValueColumn.setCellValueFactory(new PropertyValueFactory<>(""));
        contractColumn.setCellValueFactory(new PropertyValueFactory<>("contract"));
        salaryColumn.setCellValueFactory(new PropertyValueFactory<>("salary"));
        
    }
    
    
    private void initTaxiTable(){
        taxiTable = new TableView();
        
        chosenPositionColumn = new TableColumn("Position");
        firstNameColumn = new TableColumn("First Name");
        lastNameColumn = new TableColumn("Last Name");
        proTeamColumn = new TableColumn("Pro Team");
        positionsColumn = new TableColumn("Positions");
        rwColumn = new TableColumn("R/W");
        hrsvColumn = new TableColumn("HR/SV");
        rbikColumn = new TableColumn("RBI/K");
        sberaColumn = new TableColumn("SB/ERA");
        bawhipColumn = new TableColumn("BA/WHIP");
        estimatedValueColumn = new TableColumn("Estimated Value");
        contractColumn = new TableColumn("Contract");
        salaryColumn = new TableColumn("Salary");
        
        taxiTable.getColumns().add(chosenPositionColumn);
        taxiTable.getColumns().add(firstNameColumn);
        taxiTable.getColumns().add(lastNameColumn);
        taxiTable.getColumns().add(proTeamColumn);
        taxiTable.getColumns().add(positionsColumn);
        taxiTable.getColumns().add(rwColumn);
        taxiTable.getColumns().add(hrsvColumn);
        taxiTable.getColumns().add(rbikColumn);
        taxiTable.getColumns().add(sberaColumn);
        taxiTable.getColumns().add(bawhipColumn);
        taxiTable.getColumns().add(estimatedValueColumn);
        taxiTable.getColumns().add(contractColumn);
        taxiTable.getColumns().add(salaryColumn);
        
        chosenPositionColumn.setCellValueFactory(new PropertyValueFactory<>("chosenPosition"));
        firstNameColumn.setCellValueFactory(new PropertyValueFactory<>("firstName"));
        lastNameColumn.setCellValueFactory(new PropertyValueFactory<>("lastName"));
        proTeamColumn.setCellValueFactory(new PropertyValueFactory<>("proTeam"));
        positionsColumn.setCellValueFactory(new PropertyValueFactory<>("positions"));
        rwColumn.setCellValueFactory(new PropertyValueFactory<>("rw"));
        hrsvColumn.setCellValueFactory(new PropertyValueFactory<>("hrsv"));
        rbikColumn.setCellValueFactory(new PropertyValueFactory<>("rbik"));
        sberaColumn.setCellValueFactory(new PropertyValueFactory<>("sbera"));
        bawhipColumn.setCellValueFactory(new PropertyValueFactory<>("bawhip"));
        //estimatedValueColumn.setCellValueFactory(new PropertyValueFactory<>(""));
        contractColumn.setCellValueFactory(new PropertyValueFactory<>("contract"));
        salaryColumn.setCellValueFactory(new PropertyValueFactory<>("salary"));
        
    }
    public ComboBox getComboBox(){
        return selectFantasyTeamComboBox;
    }
    
    public void refreshComboBox(){
        selectFantasyTeamComboBox.getItems().clear();
                for(int i = 0; i<gui.getDataManager().getDraft().getTeamsList().size(); i++){
                    gui.getDataManager().getDraft().getTeamsList().get(i).getTeamName();
                    selectFantasyTeamComboBox.getItems().add(gui.getDataManager().getDraft().getTeamsList().get(i).getTeamName());
                }
    }
    public Button getEditTeamButton(){
        return editTeamButton;
    }
    
    public TableView<Player> getTable(){
        return lineupTable;
    }
    
    public Button getRemovePlayerButton(){
        return removePlayerButton;
    }
    
    public Button getEditPlayerContractButton(){
        return editPlayerContractButton;
    }
    
    public TableView<Player> getTaxiTable(){
        return taxiTable;
    }
    
    public Button getRemoveTaxiButton(){
        return removeTaxiButton;
    }
}
