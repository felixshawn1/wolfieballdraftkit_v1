package wbd.gui;

import wbd.data.FantasyTeam;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.layout.VBox;
import javafx.stage.Stage;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.control.TextField;
import javafx.scene.layout.FlowPane;
import javafx.scene.layout.GridPane;
import javafx.stage.Modality;
/**
 *
 * @author Shawn Felix <shawnfelixcodes.wordpress.com>
 */
public class NewFantasyTeamDialog extends Stage {
    VBox messagePane;
    Scene messageScene;
    Label headingLabel;
    
    FlowPane lineOne;
    FlowPane lineTwo;
    
    Label nameLabel;
    Label ownerLabel;
    TextField nameText;
    TextField ownerText;
    
    Button completeButton;
    Button cancelButton;
    
    /**
     * Initializes this dialog so that it can be used repeatedly
     * for all kinds of messages.
     * 
     * @param owner The owner stage of this modal dialog.
     * @param gui
     * 
     * @param closeButtonText Text to appear on the close button.
     */
    public NewFantasyTeamDialog(Stage owner, WBD_GUI gui) {
 
        this.setTitle("Add a New Fantasy Team");
        
        // MAKE IT MODAL
        initModality(Modality.WINDOW_MODAL);
        initOwner(owner);
        
        lineOne = new FlowPane();
        lineTwo = new FlowPane();
        
        // LABEL TO DISPLAY THE CUSTOM MESSAGE
        headingLabel = new Label();        
        headingLabel.setText("Fantasy Team Details");
        nameLabel = new Label();
        nameLabel.setText("Name:");
        nameLabel.getStyleClass().add("heading_label");
        nameText = new TextField();
        
        lineOne.getChildren().add(nameLabel);
        lineOne.getChildren().add(nameText);
        
        ownerLabel = new Label();
        ownerLabel.setText("Owner:");
        ownerText = new TextField();
        
        lineTwo.getChildren().add(ownerLabel);
        lineTwo.getChildren().add(ownerText);
        
        //COMPLETE BUTTON
        completeButton = new Button("Complete");
        completeButton.setOnAction(e->{ 
                        Boolean dupe = false;
                        for (int i = 0; i<gui.getDataManager().getDraft().getTeamsList().size(); i++){
                            
                                if(gui.getDataManager().getDraft().getTeamsList().get(i).getTeamName().equals(nameText.getText())){
                                    dupe = true;
                                }
                            }
                        String teamName = nameText.getText();
                        String ownerName = ownerText.getText();
                        if(teamName.equals("") || ownerName.equals("") || dupe == true){
                            //do nothing
                        }
                        else{
                        FantasyTeam newTeam = new FantasyTeam(teamName, ownerName);
                        gui.getDataManager().getDraft().getTeamsList().add(newTeam);
                        //gui.getFtScreen().getTable().setItems(newTeam.getPlayersList());
                        ownerText.clear();
                        nameText.clear();
                        NewFantasyTeamDialog.this.close();
                        }
        });
        
        // CLOSE BUTTON
        cancelButton = new Button("Cancel");
        cancelButton.setOnAction(e->{ NewFantasyTeamDialog.this.close(); });

        // WE'LL PUT EVERYTHING HERE
        messagePane = new VBox();
        messagePane.setAlignment(Pos.CENTER);
        messagePane.getChildren().add(headingLabel);
        messagePane.getChildren().add(lineOne);
        messagePane.getChildren().add(lineTwo);
        messagePane.getChildren().add(completeButton);
        messagePane.getChildren().add(cancelButton);
        
        // MAKE IT LOOK NICE
        messagePane.setPadding(new Insets(10, 20, 20, 20));
        messagePane.setSpacing(10);

        // AND PUT IT IN THE WINDOW
        messageScene = new Scene(messagePane);
        this.setScene(messageScene);
    }
 
    /**
     * This method pops open the dialog.
     * 
     * 
     */
    public void show(String message) {
        this.showAndWait();
    }
}
