package wbd.file;

import static wbd.WBD_StartupConstants.*;
import wbd.data.*;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.math.BigDecimal;
import java.time.DayOfWeek;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javax.json.Json;
import javax.json.JsonArray;
import javax.json.JsonArrayBuilder;
import javax.json.JsonObject;
import javax.json.JsonReader;
import javax.json.JsonWriter;
import javax.json.JsonValue;

/**
 *
 * @author Shawn Felix <shawnfelixcodes.wordpress.com>
 */
public class JsonDraftFileManager implements DraftFileManager {
    
    String JSON_HITTERS = "Hitters";
    String JSON_PITCHERS = "Pitchers";
    String JSON_DRAFT_NAME = "DraftName";
    String JSON_EXT = ".json";
    String JSON_PLAYERS_LIST = "playersList";
    String SLASH = "/";
    
    
     /**
     * This method saves all the data associated with a draft to
     * a JSON file.
     * 
     * @param draftToSave The draft whose data we are saving.
     * 
     * @throws IOException Thrown when there are issues writing
     * to the JSON file.
     */
    @Override
    public void saveDraft(Draft draftToSave) throws IOException {
        //BUILD THE FILE PATH
        String draftListing = "" + draftToSave.getDraftName();
        String jsonFilePath = PATH_DRAFTS + SLASH + draftListing + JSON_EXT;
        
        //INIT THE WRITER
        OutputStream os = new FileOutputStream(jsonFilePath);
        JsonWriter jsonWriter = Json.createWriter(os); 
        
        //MAKE A JSON ARRAY FOR THE PITCHERS LIST
        JsonArray pitchersJsonArray = makePitcherList(draftToSave.getPitchersList());
        
        //MAKE A JSON ARRAY FOR HTE HITTERS LIST
        JsonArray hittersJsonArray = makeHitterList(draftToSave.getHittersList());
        
        
        
        //BUILD THE DRAFT WITH EVERYTHING WE HAVE
        JsonObject draftJsonObject = Json.createObjectBuilder()
                                         .add(JSON_DRAFT_NAME, draftToSave.getDraftName())
                                         .add(JSON_PITCHERS, pitchersJsonArray)
                                         .add(JSON_HITTERS, hittersJsonArray)
                            .build();
        
        //SAVE EVERYTHING AT ONCE
        
        jsonWriter.writeObject(draftJsonObject);
    }
    
    /**
     * This method loads a draft from a draft file.
     * @param draftToLoad the new draft the user wants to load
     * @param draftFilePath the location of the draft the user wants to laod
     * @throws java.io.IOException
    */
    @Override
    public void loadDraft(Draft draftToLoad, String draftFilePath) throws IOException{
        //LOAD THE JSON FILE WITH ALL THE DATA
        JsonObject json = loadJSONFile(draftFilePath);
        
        //NOW LOAD THE DRAFT
        draftToLoad.setDraftName(json.getString(JSON_DRAFT_NAME));
        
        //SET THE FREE AGENT LIST
        draftToLoad.clearPlayerList();
        ObservableList<Player> oPlayersList = loadPlayersFromDraftFile(draftFilePath);
        draftToLoad.setPlayersList(oPlayersList);
        
        
    }
    
    //THESE METHODS LOAD PLAYERS FROM DRAFT FILES
    public ObservableList<Pitcher> loadPitchersFromDraftFile(String draftFilePath){
        try{
            ArrayList<Pitcher> list = new ArrayList();
        
            JsonObject jsonDraftFile = loadJSONFile(draftFilePath);
        
            JsonArray jsonPitchersListArray = jsonDraftFile.getJsonArray("Pitchers");
            
            for (int i = 0; i<jsonPitchersListArray.size(); i++){
                JsonObject jso = jsonPitchersListArray.getJsonObject(i);
                Pitcher p = new Pitcher();
                p.setProTeam(jso.getString("TEAM"));
                p.setLastName(jso.getString("LAST_NAME"));
                p.setFirstName(jso.getString("FIRST_NAME"));
                p.setInningsPitched(jso.getString("IP"));
                p.setEarnedRuns(jso.getString("ER"));
                p.setWins(jso.getString("W"));
                p.setSaves(jso.getString("SV"));
                p.setHits(jso.getString("H"));
                p.setBaseOnBalls(jso.getString("BB"));
                p.setStrikeouts(jso.getString("K"));
                p.setNotes(jso.getString("NOTES"));
                p.setBirthYear(jso.getString("YEAR_OF_BIRTH"));
                p.setBirthNation(jso.getString("NATION_OF_BIRTH"));
                
                p.setRw(jso.getString("W"));
                p.setHrsv(jso.getString("K"));
                
                
                //THIS SETS THE WHIP AND ERA
                p.updatePitcherValues();
                
                list.add(p);
            }
        
        ObservableList<Pitcher> output = FXCollections.observableList(list);
        
        return output;
        }catch(Exception e){
            System.out.println("FAILED TO LOAD THE JSON PITCHERS");
            return null;
        }
        
    }
    public ObservableList<Hitter> loadHittersFromDraftFile(String draftFilePath){
        try{
            ArrayList<Hitter> list = new ArrayList();
        
            JsonObject jsonDraftFile = loadJSONFile(draftFilePath);
        
            JsonArray jsonHittersListArray = jsonDraftFile.getJsonArray("Hitters");
            
            for (int i = 0; i<jsonHittersListArray.size(); i++){
                JsonObject jso = jsonHittersListArray.getJsonObject(i);
                Hitter h = new Hitter();
                h.setProTeam(jso.getString("TEAM"));
                h.setLastName(jso.getString("LAST_NAME"));
                h.setFirstName(jso.getString("FIRST_NAME"));
                //adds the CI, MI, and U positions
                String pos = jso.getString("QP");
                if(pos.contains("1B") || pos.contains("3B")){
                    pos += "_CI";
                }
                if(pos.contains("2B") || pos.contains("SS")){
                    pos += "_MI";
                }
                pos+= "U";
                System.out.println(pos);
                h.setPositions(pos);
                h.setAtBat(jso.getString("AB"));
                h.setRuns(jso.getString("R"));
                h.setHits(jso.getString("H"));
                h.setHomeRuns(jso.getString("HR"));
                h.setRbis(jso.getString("RBI"));
                h.setStolenBases(jso.getString("SB"));
                h.setNotes(jso.getString("NOTES"));
                h.setBirthYear(jso.getString("YEAR_OF_BIRTH"));
                h.setBirthNation(jso.getString("NATION_OF_BIRTH"));
                
                h.setRw(jso.getString("R"));
                h.setHrsv("HR");
                h.setRbik("RBI");
                h.setSbera("SB");
                
                //THIS SETS THE WHIP AND ERA
                h.updateHitterValues();
                
                list.add(h);
            }
        
        ObservableList<Hitter> output = FXCollections.observableList(list);
        
        return output;
        }catch(Exception e){
            System.out.println("FAILED TO LOAD THE JSON PITCHERS");
            return null;
        }
        
    }
    public ObservableList<Player> loadPlayersFromDraftFile(String draftFilePath){
        ObservableList<Pitcher> pitchers = loadPitchersFromDraftFile(draftFilePath);
        ObservableList<Hitter> hitters = loadHittersFromDraftFile(draftFilePath);
        
        //COMBINE THE TWO LISTS
        ArrayList<Player> playersList = new ArrayList<Player>(pitchers);
        playersList.addAll(hitters);
        
        ObservableList<Player> oPlayersList = FXCollections.observableList(playersList);
        return oPlayersList;
    }
    
    
    //THESE METHODS LOAD PLAYERS FROM HITTERS.JSON AND PITCHERS.JSON
    public ArrayList<Pitcher> loadPitchers(String pitchersFilePath){
        try{
            //load up the file
            JsonObject pitchersJsonFile = loadJSONFile(pitchersFilePath);
            
            //the output list of pitchers
            ArrayList<Pitcher> pOutput = new ArrayList();
            
            
            JsonArray jsonPitchersListArray = pitchersJsonFile.getJsonArray(JSON_PITCHERS);
            
            
            for (int i = 0; i<jsonPitchersListArray.size(); i++){
                JsonObject jso = jsonPitchersListArray.getJsonObject(i);
                Pitcher p = new Pitcher();
                p.setProTeam(jso.getString("TEAM"));
                p.setLastName(jso.getString("LAST_NAME"));
                p.setFirstName(jso.getString("FIRST_NAME"));
                p.setInningsPitched(jso.getString("IP"));
                p.setEarnedRuns(jso.getString("ER"));
                p.setWins(jso.getString("W"));
                p.setSaves(jso.getString("SV"));
                p.setHits(jso.getString("H"));
                p.setBaseOnBalls(jso.getString("BB"));
                p.setStrikeouts(jso.getString("K"));
                p.setNotes(jso.getString("NOTES"));
                p.setBirthYear(jso.getString("YEAR_OF_BIRTH"));
                p.setBirthNation(jso.getString("NATION_OF_BIRTH"));
                p.setRw(jso.getString("W"));
                p.setHrsv(jso.getString("SV"));
                p.setRbik(jso.getString("K"));
                p.setPositions("P");
                //THIS SETS THE WHIP AND ERA
                p.updatePitcherValues();
                
                pOutput.add(p);
            }
            
            
            return pOutput;
            
        }catch(IOException e){
            System.out.println("ERROR WITH PITCHER JSON FILE");
            return null;
        }
    }
    public ArrayList<Hitter> loadHitters(String hittersFilePath){
        try{
            JsonObject hittersJson = loadJSONFile(hittersFilePath);
        
            ArrayList<Hitter> hOutput = new ArrayList();
            
            JsonArray jsonHittersListArray = hittersJson.getJsonArray(JSON_HITTERS);
            
            
            for (int i = 0; i<jsonHittersListArray.size(); i++){
                JsonObject jso = jsonHittersListArray.getJsonObject(i);
                Hitter h = new Hitter();
                h.setProTeam(jso.getString("TEAM"));
                h.setLastName(jso.getString("LAST_NAME"));
                h.setFirstName(jso.getString("FIRST_NAME"));
                //adds the CI, MI, and U positions
                String pos = jso.getString("QP");
                if(pos.contains("1B") || pos.contains("3B")){
                    pos += "_CI";
                }
                if(pos.contains("2B") || pos.contains("SS")){
                    pos += "_MI";
                }
                pos+= "_U";
                
                h.setPositions(pos);
                h.setAtBat(jso.getString("AB"));
                h.setRuns(jso.getString("R"));
                h.setHits(jso.getString("H"));
                h.setHomeRuns(jso.getString("HR"));
                h.setRbis(jso.getString("RBI"));
                h.setStolenBases(jso.getString("SB"));
                h.setNotes(jso.getString("NOTES"));
                h.setBirthYear(jso.getString("YEAR_OF_BIRTH"));
                h.setBirthNation(jso.getString("NATION_OF_BIRTH"));
                
                h.setRw(jso.getString("R"));
                h.setHrsv(jso.getString("HR"));
                h.setRbik(jso.getString("RBI"));
                h.setSbera(jso.getString("SB"));
                
                
                
                //THIS SETS THE WHIP AND ERA
                h.updateHitterValues();
                
                hOutput.add(h);
            }
            
            
            return hOutput;
            
        }catch(IOException e){
            System.out.println("ERROR WITH HITTER JSON FILE");
            return null;
        }
    
    }
    @Override
    public ObservableList<Player> loadPlayers(String pitchersFilePath, String hittersFilePath){
        
        ArrayList<Pitcher> pitchers;
        ArrayList<Hitter> hitters;
        
        //read in the pitchers and hitters from the json file
        pitchers = loadPitchers(pitchersFilePath);
        hitters = loadHitters(hittersFilePath);
        
        ArrayList<Player> allPlayersList = new ArrayList(pitchers);
        //Combine the two lists
        allPlayersList.addAll(hitters);
        ObservableList<Player> oPList = FXCollections.observableList(allPlayersList);
        
        return oPList;
    }
    
    
    
    // LOADS AN ARRAY OF A SPECIFIC NAME FROM A JSON FILE AND
    // RETURNS IT AS AN ArrayList FULL OF THE DATA FOUND
    private ArrayList<String> loadArrayFromJSONFile(String jsonFilePath, String arrayName) throws IOException {
        JsonObject json = loadJSONFile(jsonFilePath);
        ArrayList<String> items = new ArrayList();
        JsonArray jsonArray = json.getJsonArray(arrayName);
        for (JsonValue jsV : jsonArray) {
            items.add(jsV.toString());
        }
        return items;
    }
    
    // LOADS A JSON FILE AS A SINGLE OBJECT AND RETURNS IT
    private JsonObject loadJSONFile(String jsonFilePath) throws IOException {
        InputStream is = new FileInputStream(jsonFilePath);
        JsonReader jsonReader = Json.createReader(is);
        JsonObject json = jsonReader.readObject();
        jsonReader.close();
        is.close();
        return json;
    } 
  
    

    
    
    
    
    
    
    //MAKES THE PITCHER LIST IN THE PITCHER FILE
    private JsonArray makePitcherList(ObservableList<Pitcher> pitchers){
        JsonArrayBuilder jsb = Json.createArrayBuilder();
        for(Pitcher p : pitchers){
            jsb.add(makePitcherJsonObject(p));
        }
        JsonArray jA = jsb.build();
        return jA;
    }
    
    //HELPER METHOD TO WRITE EACH PITCHER IN THE PITCHER ARRAY
    private JsonObject makePitcherJsonObject(Pitcher p){
        JsonObject jso = Json.createObjectBuilder().add("TEAM", p.getProTeam())
                                                   .add("LAST_NAME", p.getLastName())
                                                   .add("FIRST_NAME", p.getFirstName())
                                                   .add("IP", p.getInningsPitched())
                                                   .add("ER", p.getEarnedRuns())
                                                   .add("W", p.getWins())
                                                   .add("SV", p.getSaves())
                                                   .add("H", p.getHits())
                                                   .add("BB", p.getBaseOnBalls())
                                                   .add("NOTES", p.getNotes())
                                                   .add("K", p.getStrikeouts())
                                                   .add("YEAR_OF_BIRTH", p.getBirthYear())
                                                   .add("NATION_OF_BIRTH", p.getBirthNation())
                                                   .build();
        return jso;
    }
    
    //MAKES THE HITTER LIST IN THE DRAFT FILE
    private JsonArray makeHitterList(ObservableList<Hitter> hitters){
        JsonArrayBuilder jsb = Json.createArrayBuilder();
        for(Hitter h : hitters){
            jsb.add(makeHitterJsonObject(h));
        }
        JsonArray jA = jsb.build();
        return jA;
    }
    //HELPER METHOD TO WRITE EACH HITTER IN THE HITTER ARRAY 
    private JsonObject makeHitterJsonObject(Hitter h){
        JsonObject jso = Json.createObjectBuilder().add("TEAM", h.getProTeam())
                                                   .add("LAST_NAME", h.getLastName())
                                                   .add("FIRST_NAME", h.getFirstName())
                                                   .add("QP", h.getPositions())
                                                   .add("AB", h.getAtBat())
                                                   .add("R", h.getRuns())
                                                   .add("H", h.getHits())
                                                   .add("HR", h.getHomeRuns())
                                                   .add("RBI", h.getRbis())
                                                   .add("SB", h.getStolenBases())
                                                   .add("NOTES", h.getNotes())
                                                   .add("YEAR_OF_BIRTH", h.getBirthYear())
                                                   .add("NATION_OF_BIRTH", h.getBirthNation())
                                                   .build();
        return jso;
    }
    
        
    
}
