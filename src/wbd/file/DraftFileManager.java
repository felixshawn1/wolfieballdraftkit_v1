package wbd.file;

import wbd.data.Draft;
import wbd.data.Player;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import javafx.collections.ObservableList;
/**
 *
 * @author Shawn Felix <shawnfelixcodes.wordpress.com>
 */
public interface DraftFileManager {
    public void                 saveDraft(Draft draftToSave) throws IOException;  
    public void                 loadDraft(Draft draftToLoad, String draftPath) throws IOException;
    public ObservableList<Player>    loadPlayers(String pitchersFilePath, String hittersFilePath) throws IOException;
    
}
