package wbd.controller;

import wbd.gui.WBD_GUI;

/**
 *This class handles the responses to all draft
 * editing input, including verification of data and binding
 * of entered data to the Draft object.
 * 
 * @author Shawn Felix <shawnfelixcodes.wordpress.com>
 */
public class DraftEditController {
    //IF FALSE THE CONTROLLER WILL NOT RESPOND TO
    //DRAFT EDITING. IF TRUE, IT WILL.
    private boolean enabled;
    
    
    public DraftEditController(){
        enabled = true;
    }
    
    //lets us enable or disable the controller
    public void enable(boolean setting){
        enabled = setting;
    }
    
    public void handleDraftChangeRequest(WBD_GUI gui){
        if (enabled){
            try{
                //UPDATE THE DRAFT
                gui.updateDraftInfo(gui.getDataManager().getDraft());
                
                //THE DRAFT IS NOW CHANGED
                gui.getFileController().markAsEdited(gui);
                
            }catch(Exception e){
                System.out.println("PROBLEM HANDLING THE DRAFT CHANGE REQUEST");
            }
        }
        
    }
}
