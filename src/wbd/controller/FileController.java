package wbd.controller;

import static wbd.WBD_PropertyType.COURSE_SAVED_MESSAGE;
import static wbd.WBD_PropertyType.NEW_COURSE_CREATED_MESSAGE;
import static wbd.WBD_PropertyType.SAVE_UNSAVED_WORK_MESSAGE;
import static wbd.WBD_StartupConstants.JSON_FILE_PATH_LAST_INSTRUCTOR;
import static wbd.WBD_StartupConstants.PATH_DRAFTS;
import wbd.data.Draft;
import wbd.data.DraftDataManager;
//import wbd.data.DraftPage;
//import wbd.data.Instructor;
import wbd.error.ErrorHandler;
import wbd.file.DraftFileManager;
import wbd.file.DraftSiteExporter;
import wbd.gui.WBD_GUI;
import wbd.gui.MessageDialog;
import wbd.gui.YesNoCancelDialog;
//import wbd.gui.WebBrowser;
import java.io.File;
import java.io.IOException;
import java.net.MalformedURLException;
import java.util.ArrayList;
import java.util.Locale;
import static javafx.application.Application.launch;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.stage.Stage;
import javafx.stage.FileChooser;
import properties_manager.PropertiesManager;
import static wbd.WBD_StartupConstants.JSON_FILE_PATH_HITTERS;
import static wbd.WBD_StartupConstants.JSON_FILE_PATH_PITCHERS;
import static wbd.WBD_StartupConstants.PATH_DRAFTS;
import wbd.data.Player;
import wbd.gui.NewFantasyTeamDialog;

/**
 *
 * @author Shawn Felix <shawnfelixcodes.wordpress.com>
 */
public class FileController {
    //KEEP TRACK OF WHEN SOMETHING IS NOT SAVED
    boolean saved;
    
    //READ AND WRITE DRAFT DATA
    private DraftFileManager draftIO;
    
    //@todo
    //private DraftSiteExporter exporter;
    
    //THIS WILL PROVIDE FEEDBACK
    ErrorHandler errorHandler;
    
    //PROVIDES FEEDBACK TO THE USER AFTER WORK BY THIS CLASS HAS FINISHED
    MessageDialog messageDialog;
    
    //USED FOR ADDING A TEAM TO THE DRAFT
    NewFantasyTeamDialog newFantasyTeamDialog;
    
    // AND WE'LL USE THIS TO ASK YES/NO/CANCEL QUESTIONS
    YesNoCancelDialog yesNoCancelDialog;
    
    // WE'LL USE THIS TO GET OUR VERIFICATION FEEDBACK
    PropertiesManager properties;
    
    
    public FileController(
            MessageDialog initMessageDialog, 
            YesNoCancelDialog initYesNoCancelDialog,
            NewFantasyTeamDialog newFantasyTeamDialog,
            DraftFileManager initDraftIO){
        saved = true;
        
        draftIO = initDraftIO;
        
        //BE READY FOR ERRORS
        errorHandler = ErrorHandler.getErrorHandler();
        
        //PROVIDE FEEDBACK
        messageDialog = initMessageDialog;
        yesNoCancelDialog = initYesNoCancelDialog;
        properties = PropertiesManager.getPropertiesManager();
        
    }
    
    public void markAsEdited(WBD_GUI gui){
        //THE DRAFT OBJECT IS NOW CHANGED
        saved = false;
        //let the ui know
        gui.updateToolbarControls(saved);
    }
    
    
    public void handleNewDraftRequest(WBD_GUI gui){
        try{
            boolean continueToMakeNew = true;
            if (!saved) {
                // THE USER CAN OPT OUT HERE WITH A CANCEL
                continueToMakeNew = promptToSave(gui);
            }

            // IF THE USER REALLY WANTS TO MAKE A NEW COURSE
            
            if (continueToMakeNew) {
            
                // RESET THE DATA, WHICH SHOULD TRIGGER A RESET OF THE UI
                DraftDataManager dataManager = gui.getDataManager();
                dataManager.reset();
                saved = false;
                
                
                // REFRESH THE GUI, WHICH WILL ENABLE AND DISABLE
                // THE APPROPRIATE CONTROLS
                gui.updateToolbarControls(saved);
                
                
                // TELL THE USER THE COURSE HAS BEEN CREATED
                messageDialog.show(properties.getProperty(NEW_COURSE_CREATED_MESSAGE));
            }
        }   catch(IOException ioe){
            errorHandler.handleNewDraftError();
        }
    }
    
    public void handleLoadDraftRequest(WBD_GUI gui){
        try {
            // WE MAY HAVE TO SAVE CURRENT WORK
            boolean continueToOpen = true;
            if (!saved) {
                // THE USER CAN OPT OUT HERE WITH A CANCEL
                continueToOpen = promptToSave(gui);
            }

            // IF THE USER REALLY WANTS TO OPEN A Course
            if (continueToOpen) {
                // GO AHEAD AND PROCEED LOADING A Course
                promptToOpen(gui);
            }
        } catch (IOException ioe) {
            // SOMETHING WENT WRONG
            //errorHandler.handleLoadDraftError();
            System.out.println("-----SOMETHING WENT WRONG WITH LOADING THE DRAFT------");
        }
    }
    
    public void handleSaveDraftRequest(WBD_GUI gui, Draft draftToSave){
        try {
            // SAVE IT TO A FILE
            draftIO.saveDraft(draftToSave);

            // MARK IT AS SAVED
            saved = true;

            // TELL THE USER THE FILE HAS BEEN SAVED
            messageDialog.show("Your draft has been saved");

            // AND REFRESH THE GUI, WHICH WILL ENABLE AND DISABLE
            // THE APPROPRIATE CONTROLS
            gui.updateToolbarControls(saved);
        } catch (IOException ioe) {
            //errorHandler.handleSaveCourseError();
            System.out.println("THERE WAS A PROBLEM SAVING YOUR DRAFT");
        }
    }
    
    /**
     * This method handles the Exit button request
     */
    public void handleExitRequest(WBD_GUI gui){
        try {
            // WE MAY HAVE TO SAVE CURRENT WORK
            boolean continueToExit = true;
            if (!saved) {
                // THE USER CAN OPT OUT HERE
                continueToExit = promptToSave(gui);
            }

            // IF THE USER REALLY WANTS TO EXIT THE APP
            if (continueToExit) {
                // EXIT THE APPLICATION
                System.exit(0);
            }
        } catch (IOException ioe) {
            ErrorHandler eH = ErrorHandler.getErrorHandler();
            //eH.handleExitError();
        }
    }
    
    /**
     * This helper method verifies that the user really wants to save their
     * unsaved work, which they might not want to do. Note that it could be used
     * in multiple contexts before doing other actions, like creating a new
     * Course, or opening another Course. Note that the user will be
     * presented with 3 options: YES, NO, and CANCEL. YES means the user wants
     * to save their work and continue the other action (we return true to
     * denote this), NO means don't save the work but continue with the other
     * action (true is returned), CANCEL means don't save the work and don't
     * continue with the other action (false is returned).
     *
     * @return true if the user presses the YES option to save, true if the user
     * presses the NO option to not save, false if the user presses the CANCEL
     * option to not continue.
     */
     private boolean promptToSave(WBD_GUI gui) throws IOException {
        // PROMPT THE USER TO SAVE UNSAVED WORK
        yesNoCancelDialog.show(properties.getProperty(SAVE_UNSAVED_WORK_MESSAGE));
        
        // AND NOW GET THE USER'S SELECTION
        String selection = yesNoCancelDialog.getSelection();

        // IF THE USER SAID YES, THEN SAVE BEFORE MOVING ON
        if (selection.equals(YesNoCancelDialog.YES)) {
            // SAVE THE COURSE
            DraftDataManager dataManager = gui.getDataManager();
            draftIO.saveDraft(dataManager.getDraft());
            saved = true;
            
            
        } // IF THE USER SAID CANCEL, THEN WE'LL TELL WHOEVER
        // CALLED THIS THAT THE USER IS NOT INTERESTED ANYMORE
        else if (selection.equals(YesNoCancelDialog.CANCEL)) {
            return false;
        }

        // IF THE USER SAID NO, WE JUST GO ON WITHOUT SAVING
        // BUT FOR BOTH YES AND NO WE DO WHATEVER THE USER
        // HAD IN MIND IN THE FIRST PLACE
        return true;
    }
     
      /**
     * This helper method asks the user for a file to open. The user-selected
     * file is then loaded and the GUI updated. Note that if the user cancels
     * the open process, nothing is done. If an error occurs loading the file, a
     * message is displayed, but nothing changes.
     */
    private void promptToOpen(WBD_GUI gui) {
        // AND NOW ASK THE USER FOR THE COURSE TO OPEN
        FileChooser courseFileChooser = new FileChooser();
        courseFileChooser.setInitialDirectory(new File(PATH_DRAFTS));
        File selectedFile = courseFileChooser.showOpenDialog(gui.getWindow());

        // ONLY OPEN A NEW FILE IF THE USER SAYS OK
        if (selectedFile != null) {
            try {
                Draft draftToLoad = gui.getDataManager().getDraft();
                draftIO.loadDraft(draftToLoad, selectedFile.getAbsolutePath());
                gui.reloadDraft(draftToLoad);
                saved = true;
                gui.updateToolbarControls(saved);
            } catch (Exception e) {
                ErrorHandler eH = ErrorHandler.getErrorHandler();
                //eH.handleLoadCourseError();
            }
        }
    }
     
     
     
    public void handleFantasyStandingsRequest(WBD_GUI gui){
        gui.allScreensPane.setCenter(gui.fantasyStandingsPane);
    }
    
    public void handleAvailablePlayersRequest(WBD_GUI gui){
        gui.allScreensPane.setCenter(gui.availablePlayersPane);
    }
    
    public void handleFantasyTeamsRequest(WBD_GUI gui){
        gui.allScreensPane.setCenter(gui.fantasyTeamsPane);
    }
    
    public void handleDraftSummaryRequest(WBD_GUI gui){
        gui.allScreensPane.setCenter(gui.draftSummaryPane);
    }
    
    public void handleMlbTeamsRequest(WBD_GUI gui){
        gui.allScreensPane.setCenter(gui.mlbTeamsPane);
    }
    

    
}
